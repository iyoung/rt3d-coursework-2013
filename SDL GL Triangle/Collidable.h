#pragma once
#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <vector>
typedef glm::vec3 point3;
//struct used by collidables to easily commicate their collidable vertices to host programs.
struct BoundingBox
{
	std::vector<point3> axes;
	std::vector<point3> points;
	int numAxes;
	void update(point3 newPoint, point3 oldPoint,float angleX,float angleY,float angleZ)
	{
		//translate to origin
		for(size_t i = 0; i < points.size();i++)
		{
			points[i]-=oldPoint;
		}
		//rotate points and axes
		for(size_t i = 0; i < points.size();i++)
		{
			points[i]=glm::rotate(points[i],angleX,points[i]);
			points[i]=glm::rotate(points[i],angleY,points[i]);
			points[i]=glm::rotate(points[i],angleZ,points[i]);
		}
		for(size_t i = 0; i < axes.size();i++)
		{
			axes[i]=glm::rotate(axes[i],angleX,axes[i]);
			axes[i]=glm::rotate(points[i],angleY,axes[i]);
			axes[i]=glm::rotate(points[i],angleZ,axes[i]);
		}
		//translate back to position
		for(size_t i = 0; i < points.size();i++)
		{
			points[i]+=newPoint;
		}
	}
	
};
////////////////////////////////////////////////////////////////////
// This interface will deal with all physical interactions	      //
// And aids in detecting if the concrete classes are near		  //
// an area, contains a position, or have an intersection		  //
// in bounds of geometry.									      //
////////////////////////////////////////////////////////////////////
class Collidable
{
public:
	Collidable(void);
	virtual BoundingBox* getBounds()=0;
	virtual void initBounds()=0;
	virtual float getRadius()=0;
	virtual ~Collidable(void);
};

