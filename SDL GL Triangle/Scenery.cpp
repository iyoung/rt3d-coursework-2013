#include "Scenery.h"


Scenery::Scenery(void)
{
	mBounds = NULL;
	mOrientateAngle=0.0;
}
void Scenery::setDimensions(float height, float width, float length)
{
	mHeight = height;
	mWidth = width;
	mLength = length;
	initBounds();
}
vector3d Scenery::getOrientation()
{
	return mOrientateVector;
}
void Scenery::render(RenderMode renderSelect,glm::mat4 projection)
{
	glUseProgram(shaderProgram);
	glCullFace(GL_BACK);
	glm::mat4 view(1.0);
	mCamera->setViewMatrix(view);
	rt3d::setEmissiveMaterial(shaderProgram,*mMaterial);
	rt3d::setUniformMatrix4fv(shaderProgram,"projection",glm::value_ptr(projection));
	glm::mat4 model(1.0);
	model = glm::translate(model,mPosition);
	model = glm::rotate(model,mOrientateAngle,glm::vec3(0.0,1.0,0.0));
	model = glm::scale(model,glm::vec3(mWidth,mHeight,mLength));
	model = view*model;
	rt3d::setUniformMatrix4fv(shaderProgram,"modelview",glm::value_ptr(model));
	rt3d::setLight(shaderProgram,*mLight);


	glm::vec4 tmp = model* glm::vec4(mLight->position[0],mLight->position[1],mLight->position[2],mLight->position[3]);

	rt3d::setLightPos(shaderProgram, glm::value_ptr(tmp));

	glBindTexture(GL_TEXTURE_2D,mTexture);
	rt3d::drawMesh(mMesh,mNumVerts,GL_TRIANGLES);
	if(renderSelect == DEBUG)
	{
		rt3d::drawMesh(mBoundsCube,8,GL_LINES);
	}
}
void Scenery::rotate(float angle)
{
	//increment orientation by input
	mOrientateAngle += angle;
	mBounds->update(mPosition,mPosition,0.0,mOrientateAngle,0.0);
	mOrientateVector.x = cos(mOrientateAngle);
	mOrientateVector.z = sin(mOrientateAngle);
	mOrientateVector.y = 0.0;
}
void Scenery::initBounds()
{
		//character needs a bounding box as a requirement of implimenting collidable interface.
	if (mBounds!=NULL)
		delete mBounds;
	mBounds = new BoundingBox();
	//generate new vertices which will be used as collision points.
	mBounds->points.push_back(point3(mPosition.x+mWidth/2.0f, mPosition.y+mHeight, mPosition.z+mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x-mWidth/2.0f, mPosition.y+mHeight, mPosition.z+mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x-mWidth/2.0f, mPosition.y,			mPosition.z+mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x+mWidth/2.0f, mPosition.y,			mPosition.z+mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x+mWidth/2.0f, mPosition.y+mHeight, mPosition.z-mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x-mWidth/2.0f, mPosition.y+mHeight, mPosition.z-mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x-mWidth/2.0f, mPosition.y,			mPosition.z-mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x+mWidth/2.0f, mPosition.y,			mPosition.z-mLength/2.0f));
	//as this is a parallel cuboid all we need do is store 3 axes, and normalise them, for accuracy.
	mBounds->axes.push_back(glm::normalize(mBounds->points[0]-mBounds->points[1]));
	mBounds->axes.push_back(glm::normalize(mBounds->points[2]-mBounds->points[4]));
	mBounds->axes.push_back(glm::normalize(mBounds->points[0]-mBounds->points[3]));
	//finally set number of axis to test for this bounding box.
	mBounds->numAxes=mBounds->axes.size();
	mBoundsCube = rt3d::createMesh(8,(GLfloat*)mBounds->points.data());
}
void Scenery::initGFX(GLuint shader, GLuint texture, GLuint mesh, GLuint numVerts, Camera* camera, rt3d::lightStruct* light, rt3d::materialStruct* material)
{
	shaderProgram = shader;
	mMesh = mesh;
	mNumVerts = numVerts;
	mCamera = camera;
	mLight = light;
	mMaterial = material;
	mTexture = texture;
}
void Scenery::moveTo(point3 iPosition)
{

	//move this objects position, and update
	mBounds->update(iPosition,mPosition,0.0f,mOrientateAngle,0.0f);
	mPosition = iPosition;
}
void Scenery::moveTo(float iX, float iY, float iZ)
{
	//move this objects position
	point3 tPosition= glm::vec3(iX,iY,iZ);
	mBounds->update(tPosition,mPosition,0.0f,mOrientateAngle,0.0f);
	mPosition = tPosition;
}
Scenery::~Scenery(void)
{
}
