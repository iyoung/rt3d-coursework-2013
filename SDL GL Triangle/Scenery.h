#pragma once
#include "renderable.h"
#include "Collidable.h"
#include "WorldObject.h"
#include "GFX_Object.h"
#include "Camera.h"
class Scenery :
	public Renderable, public Collidable, public WorldObject, public GFX_Object
{
public:
	Scenery(void);
	//FIX ME: interface implementations
	virtual void setDimensions(float height, float width, float length);
	virtual void render(RenderMode renderSelect,glm::mat4 projection);
	virtual void initGFX(GLuint shader, GLuint texture, GLuint mesh, GLuint numVerts, Camera* camera,rt3d::lightStruct* light, rt3d::materialStruct* material);
	virtual void moveTo(point3 iPosition);
	virtual void rotate(float angle);
	virtual void moveTo(float iX, float iY, float iZ);
	virtual vector3d getOrientation();
	virtual BoundingBox* getBounds(){return mBounds;}
	virtual void initBounds();
	virtual float getRadius(){return 0;}
	virtual point3 getLocation(){return mPosition;}
	~Scenery(void);
private:
	GLuint shaderProgram;
	point3 mPosition;
	float mOrientateAngle;
	vector3d mOrientateVector;
	GLuint mTexture;
	GLuint mNumVerts;
	Camera* mCamera;
	rt3d::lightStruct* mLight;
	rt3d::materialStruct* mMaterial;
	GLuint mMesh;
	BoundingBox* mBounds;
	GLuint mBoundsCube;
	float mHeight;
	float mWidth;
	float mLength;
};

