#include "LightObject.h"


LightObject::LightObject(void)
{
}
void LightObject::init()
{
	light = new rt3d::lightStruct();
}
void LightObject::render(RenderMode renderSelect,glm::mat4 projection)
{
	rt3d::setLight(3,*light);
}
void LightObject::setAmbient(float R, float G, float B, float A)
{
	light->ambient[0]=R;
	light->ambient[1]=G;
	light->ambient[2]=B;
	light->ambient[3]=A;
}
void LightObject::setDiffuse(float R, float G, float B, float A)
{
	light->diffuse[0]=R;
	light->diffuse[1]=G;
	light->diffuse[2]=B;
	light->diffuse[3]=A;

}
void LightObject::setSpecular(float R, float G, float B, float A)
{
	light->specular[0]=R;
	light->specular[1]=G;
	light->specular[2]=B;
	light->specular[3]=A;
}

void LightObject::setPosition(float X, float Y, float Z, float W)
{
	light->position[0]=X;
	light->position[1]=Y;
	light->position[2]=Z;
	light->position[3]=W;

}
void LightObject::move()
{
	//this will have motion calculations for moving the light across the sky

}
void LightObject::moveTo(float iX, float iY, float iZ)
{
	light->position[0]=iX;
	light->position[1]=iY;
	light->position[2]=iZ;
}
void LightObject::moveTo(glm::vec3 iPosition)
{
	light->position[0]=iPosition.x;
	light->position[1]=iPosition.y;
	light->position[2]=iPosition.z;
}
point3d LightObject::getLocation()
{
	return point3d(light->position[0],light->position[1],light->position[2]);
}
vector3d LightObject::getOrientation()
{
	return orientation;
}
void LightObject::rotate(float angle)
{
	//this wont be used in this class
}
LightObject::~LightObject(void)
{
}
