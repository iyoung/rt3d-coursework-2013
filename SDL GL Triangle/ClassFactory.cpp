#include "ClassFactory.h"
#include <glm/glm.hpp>
#include <istream>
#include <sstream>

ClassFactory::ClassFactory(void)
{
	worldCamera=NULL;
}
void ClassFactory::init()
{
	defaultShader = rt3d::initShaders("phong-tex.vert","phong-tex.frag");
	//defaultShader = rt3d::initShaders("phong-tex.vert","phong-tex.frag");
	//these need to be changed to appropriate shaders
	skyboxShader = rt3d::initShaders("phong-tex.vert","phong-tex.frag");
	characterShader = rt3d::initShaders("phong-tex.vert","phong-tex.frag");
	ObjLoader = new OBJFileLoader();
}
Skybox* ClassFactory::getSkyBox(std::string skyboxDirectory)
{
	Skybox* skybox = new Skybox();
	
	std::string temp;
	char lineData[256];
	std::vector<std::string*> fileData;
	std::ifstream file;
	//open the specified file
	file.open(skyboxDirectory);
	if (file)
	{
		while ( file.good() )
		{
			file.getline(lineData,256);
			fileData.push_back(new std::string(lineData));
		}
	}
	else
	{
		std::cout << "unable to open: " << skyboxDirectory;
	}
	file.close();
	file.close();
	for (size_t i = 0; i<fileData.size();++i) 
	{
		if(fileData[i]->c_str()[0]=='S')
		{
			float size;
			std::string tmpS;
			std::istringstream ss(fileData[i]->c_str());
			ss>>tmpS>>size;
			skybox->setSize(size);
			temp.clear();
		}
		if(fileData[i]->c_str()[0]=='T')
		{
			std::string tmpS;
			std::istringstream ss(fileData[i]->c_str());
			ss>>tmpS>>temp;
			skybox->setFaceImage(UP,(char*)temp.c_str());
			temp.clear();
		}
		if(fileData[i]->c_str()[0]=='L')
		{
			std::string tmpS;
			std::istringstream ss(fileData[i]->c_str());
			ss>>tmpS>>temp;
			skybox->setFaceImage(LEFT,(char*)temp.c_str());
			temp.clear();
		}
		if(fileData[i]->c_str()[0]=='R')
		{
			std::string tmpS;
			std::istringstream ss(fileData[i]->c_str());
			ss>>tmpS>>temp;
			skybox->setFaceImage(RIGHT,(char*)temp.c_str());
			temp.clear();
		}
		if(fileData[i]->c_str()[0]=='F')
		{
			std::string tmpS;
			std::istringstream ss(fileData[i]->c_str());
			ss>>tmpS>>temp;
			skybox->setFaceImage(FRONT,(char*)temp.c_str());
			temp.clear();
		}
		if(fileData[i]->c_str()[0]=='B')
		{
			std::string tmpS;
			std::istringstream ss(fileData[i]->c_str());
			ss>>tmpS>>temp;
			skybox->setFaceImage(BACK,(char*)temp.c_str());
			temp.clear();
		}
	}
	
	skybox->initGFX(skyboxShader,0,0,0,worldCamera,worldLight,nullptr);
	return skybox;

}
Character* ClassFactory::getCharacter(std::string charFname)
{
	glm::vec3 location;
	std::string name;
	float strength; float speed; int hp; float mass;
	float height; float width; float length;
	std::string texFName;
	std::string meshFName;
	std::string temp;
	char lineData[256];
	std::vector<std::string*> fileData;
	float orientation;
	//TODO Parse data files to create class
	std::ifstream file;
	//open the specified file
	file.open(charFname,std::ios::in);
	if (file.is_open())
	{
		while ( file.good() )
		{
			file.getline(lineData,256);
			fileData.push_back(new std::string(lineData));
		}
	}
	file.close();
	for (size_t i = 0; i<fileData.size();++i) 
	{
		//character name
		if(fileData[i]->c_str()[0]=='n')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>name;
			ss.clear();
		}
		//locational data
		if(fileData[i]->c_str()[0]=='L')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>location.x>>location.y>>location.z;
			ss.clear();
		}
		//health
		if(fileData[i]->c_str()[0]=='h')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>hp;
			ss.clear();
		}
		//speed
		if(fileData[i]->c_str()[0]=='S')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>speed;
			ss.clear();
		}
		//strength
		if(fileData[i]->c_str()[0]=='s')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>strength;
			ss.clear();
		}
		//mass
		if(fileData[i]->c_str()[0]=='m')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>mass;
			ss.clear();
		}
		//dimensions
		if(fileData[i]->c_str()[0]=='d')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>height>>width>>length;
			ss.clear();
		}
		if(fileData[i]->c_str()[0]=='V')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>meshFName;
			ss.clear();
		}
		if(fileData[i]->c_str()[0]=='T')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>texFName;
			ss.clear();
		}
		//orientation
		if(fileData[i]->c_str()[0]=='O')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>orientation;
			ss.clear();
		}
	}
	fileData.clear();
	//parse character data file and assign values
	Character* character = new Character();
	char* tmpCP =const_cast<char*> (texFName.c_str());
	rt3d::materialStruct* material = new rt3d::materialStruct();
	material->setAmbient(0.5f,0.5f,0.5f,1.0f);
	material->setDiffuse(0.6f,0.6f,0.6f,1.0f);
	material->setEmissive(0.0f,0.0f,0.0f,0.0f);
	material->setSpecular(0.9f,0.9f,0.9f,1.0f);
	material->setShininess(5.0f);
	GLuint tti = rt3d::loadBitmap(tmpCP);
	character->initGFX(characterShader,tti,0,meshFName,worldCamera,worldLight,material);
	character->setHealth(hp);
	character->setMass(mass);
	character->setSpeed(speed);
	character->setStrength(strength);
	character->setDimensions(height,width,length);
	character->moveTo(location+glm::vec3(0.0,0.0,0.0));
	character->rotate(orientation);
	//needs light, material, and camera


	worldObjects.push_back(character);
	//and clean up temp data
	meshVerts.clear();
	meshNormals.clear();
	meshIndices.clear();
	texCoords.clear();
	return character;
}
Monster* ClassFactory::getMonster(std::string monsterFname)
{
	glm::vec3 location;
	float strength; float speed; int hp; float mass;
	float height; float width; float length;
	std::string name;
	std::string temp;
	std::string texFName;
	std::string meshFName;
	std::ifstream file;
	std::vector<std::string*> fileData;
	char lineData[256];
	//open the specified file
	file.open(monsterFname,std::ios::in);
	if (file.is_open())
	{
		while ( file.good() )
		{
			file.getline(lineData,256);
			fileData.push_back(new std::string(lineData));
		}
	}
	file.close();

	for (size_t i = 0; i<fileData.size();++i) 
	{
		//character name
		if(fileData[i]->c_str()[0]=='n')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>name;
			ss.clear();
		}
		//locational data
		if(fileData[i]->c_str()[0]=='L')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>location.x>>location.y>>location.z;
			ss.clear();
		}
		//health
		if(fileData[i]->c_str()[0]=='h')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>hp;
			ss.clear();
		}
		//speed
		if(fileData[i]->c_str()[0]=='S')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>speed;
			ss.clear();
		}
		//strength
		if(fileData[i]->c_str()[0]=='s')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>strength;
			ss.clear();
		}
		//mass
		if(fileData[i]->c_str()[0]=='m')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>mass;
			ss.clear();
		}
		//dimensions
		if(fileData[i]->c_str()[0]=='d')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>height>>width>>length;
			ss.clear();
		}
		if(fileData[i]->c_str()[0]=='V')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>meshFName;
			ss.clear();
		}
		if(fileData[i]->c_str()[0]=='T')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>texFName;
			ss.clear();
		}
	}
	fileData.clear();
	//parse data file to create class
	
	Monster* monster = new Monster();

	//needs material
	rt3d::materialStruct* material = new rt3d::materialStruct();
	material->setAmbient(0.5f,0.5f,0.5f,1.0f);
	material->setDiffuse(0.6f,0.6f,0.6f,1.0f);
	material->setEmissive(0.0f,0.0f,0.0f,0.0f);
	material->setSpecular(0.9f,0.9f,0.9f,1.0f);
	material->setShininess(5.0f);
	//init graphics components
	char* tmpCP = const_cast<char*> (texFName.c_str());
	md2model* tcm = new md2model();
	GLuint tmi = tcm->ReadMD2Model(meshFName.c_str());
	monster->initGFX(characterShader,rt3d::loadBitmap(tmpCP),0,meshFName,worldCamera,worldLight,material);
	//set world data and add to managed world objects.
	
	monster->setHealth(hp);
	monster->setMass(mass);
	monster->setSpeed(speed);
	monster->setStrength(strength);
	monster->setDimensions(height,width,length);
	monster->moveTo(location);
	worldObjects.push_back(monster);
	//and clean up temp data
	meshVerts.clear();
	meshNormals.clear();
	meshIndices.clear();
	texCoords.clear();
	return monster;
	
}
Scenery* ClassFactory::getScenery(std::string sceneFname)
{
	//parse data files to create class
	float height; float width; float length;
	float orientation;
	point3 location;
	std::string meshFName;
	std::string texFName;

	std::string temp;

	std::ifstream file;
	std::vector<std::string*> fileData;
	char lineData[256];
	//open the specified file
	file.open(sceneFname,std::ios::in);
	if (file.is_open())
	{
		while ( file.good() )
		{
			file.getline(lineData,256);
			fileData.push_back(new std::string(lineData));
		}
	}
	file.close();
	for (size_t i = 0; i<fileData.size();++i) 
	{

		//character name
		if(fileData[i]->c_str()[0]=='L')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>location.x>>location.y>>location.z;
			ss.clear();
		}

		//locational data
		if(fileData[i]->c_str()[0]=='O')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>orientation;
			ss.clear();
		}

		//dimensions
		if(fileData[i]->c_str()[0]=='w')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>width;
			ss.clear();
		}
		if(fileData[i]->c_str()[0]=='h')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>height;
			ss.clear();
		}
		if(fileData[i]->c_str()[0]=='l')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>length;
			ss.clear();
		}

		//mesh file name
		if(fileData[i]->c_str()[0]=='m')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>meshFName;
			ss.clear();
		}

		//texture filename
		if(fileData[i]->c_str()[0]=='t')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>texFName;
			ss.clear();
		}
	}
	//init graphics components
	std::vector<GLuint> indices;
	rt3d::loadObj(meshFName.c_str(),meshVerts,meshNormals,texCoords,indices);
	//ObjLoader->loadOBJ(meshFName.c_str(),meshVerts,meshNormals,meshIndices,texCoords);
	GLuint meshID=rt3d::createMesh(meshVerts.size()/3.0,meshVerts.data(),nullptr,meshNormals.data(),texCoords.data());
	GLuint numVerts = meshVerts.size()/3.0;
	rt3d::materialStruct* material = new rt3d::materialStruct();
	material->setAmbient(0.5f,0.5f,0.5f,1.0f);
	material->setDiffuse(0.6f,0.6f,0.6f,1.0f);
	material->setEmissive(0.0f,0.0f,0.0f,0.0f);
	material->setSpecular(0.9f,0.9f,0.9f,1.0f);
	material->setShininess(1.0f);
	Scenery* scenery = new Scenery();
	char* tmpCP = const_cast<char*> (texFName.c_str());
	scenery->initGFX(defaultShader,rt3d::loadBitmap(tmpCP),meshID,numVerts,worldCamera,worldLight,material);
	scenery->setDimensions(height, width, length);
	//set world data and add to managed world objects.
	scenery->moveTo(location+glm::vec3(0.0,0,0.0));
	scenery->rotate(orientation);
	worldObjects.push_back(scenery);
	//and clean up temp data
	meshVerts.clear();
	meshNormals.clear();
	meshIndices.clear();
	texCoords.clear();
	return scenery;
}
Collectable* ClassFactory::getCollectable(std::string collFname)
{
	//TODO parse data files to create class
	glm::vec3 location;
	int hpb; float strB; float speedB;
	float size;
	std::string meshFName;
	std::string texFName;
	Collectable* collectable = new Collectable();
	std::string temp;
	std::ifstream file;
	std::vector<std::string*> fileData;
	char lineData[256];
	//open the specified file
	file.open(collFname,std::ios::in);
	if (file.is_open())
	{
		while ( file.good() )
		{
			file.getline(lineData,256);
			fileData.push_back(new std::string(lineData));
		}
	}
	file.close();
	for (size_t i = 0; i<fileData.size();++i) 
	{
		
		//locational data
		if(fileData[i]->c_str()[0]=='L')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>location.x>>location.y>>location.z;
			ss.clear();
		}
		//health
		if(fileData[i]->c_str()[0]=='h')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>hpb;
			ss.clear();
		}
		//speed
		if(fileData[i]->c_str()[0]=='S')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>speedB;
			ss.clear();
		}
		//strength
		if(fileData[i]->c_str()[0]=='s')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>strB;
			ss.clear();
		}
		//dimensions
		if(fileData[i]->c_str()[0]=='d')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>size;
			ss.clear();
		}
		//texture
		if(fileData[i]->c_str()[0]=='t')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>texFName;
			ss.clear();
		}
		//
		if(fileData[i]->c_str()[0]=='m')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>meshFName;
			ss.clear();
		}
	}
	fileData.clear();
	//filename for mesh should be from parsed data
	//init graphics components
	char* meshCP = const_cast<char*> (meshFName.c_str());
	std::vector<GLuint> indices;
	rt3d::loadObj(meshCP,meshVerts,meshNormals,texCoords,indices);
	GLuint meshID=rt3d::createMesh(meshVerts.size(),meshVerts.data(),nullptr,meshNormals.data(),texCoords.data());
	GLuint numVerts = meshVerts.size();
	rt3d::materialStruct* material = new rt3d::materialStruct();
	material->setAmbient(0.5f,0.1f,0.5f,1.0f);
	material->setDiffuse(0.6f,0.1f,0.6f,1.0f);
	material->setEmissive(0.0f,0.0f,0.0f,0.0f);
	material->setSpecular(0.9f,0.1f,0.9f,1.0f);
	material->setShininess(100.0f);
	char* tmpCP =const_cast<char*> (texFName.c_str());
	collectable->initGFX(defaultShader,rt3d::loadBitmap(tmpCP),meshID, numVerts,worldCamera,worldLight, material);
	//set world data and add to managed world objects.
	collectable->setHealthBoost(hpb);
	collectable->setStrengthBoost(strB);
	collectable->setSpeedBoost(speedB);
	collectable->setDimensions(size,size,size);
	collectable->moveTo(location);
	worldObjects.push_back(collectable);
	//and clean up temp data
	meshVerts.clear();
	meshNormals.clear();
	meshIndices.clear();
	texCoords.clear();

	return collectable;
}
LightObject* ClassFactory::getLight(std::string lightfName)
{
	std::string temp;
	std::ifstream file;
	std::vector<std::string*> fileData;
	char lineData[256];
	file.open(lightfName,std::ios::in);

	if (file.is_open())
	{
		while ( file.good() )
		{
			file.getline(lineData,256);
			fileData.push_back(new std::string(lineData));
		}
	}
	file.close();

	//parse fileData
	float r; float g; float b; float a;
	float x; float y; float z; float w;
	LightObject* light = new LightObject();
	light->init();
	for(size_t i = 0; i < fileData.size();i++)
	{
		if(fileData[i]->c_str()[0] == 'a')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>r>>g>>b>>a;
			light->setAmbient(r,g,b,a);
			ss.clear();
		}
		if(fileData[i]->c_str()[0] == 'd')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>r>>g>>a;
			light->setDiffuse(r,g,b,a);
			ss.clear();
		}
		if(fileData[i]->c_str()[0] == 's')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>r>>g>>a;
			light->setSpecular(r,g,b,a);
			ss.clear();
		}
		if(fileData[i]->c_str()[0] == 'p')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>x>>y>>z>>w;
			light->setPosition(x,y,z,w);
			ss.clear();
		}
	}
	//parse light file and assign values to ambient
	//store a pointer to this object
	worldObjects.push_back(light);
	worldLight = light->getLightData();
	//and complete request
	return light;
}
Camera* ClassFactory::getCamera()
{
	if(worldCamera == NULL)
		worldCamera =  new Camera();
	return worldCamera;
}
ClassFactory::~ClassFactory(void)
{
	worldObjects.clear();
	skyBoxes.clear();
	meshVerts.clear();
	meshNormals.clear();
	meshIndices.clear();
	texCoords.clear();

	delete ObjLoader;
}
