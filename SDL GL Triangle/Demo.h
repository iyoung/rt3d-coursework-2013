#pragma once
#include "DemoWorld.h"
#include "Controller.h"
#include "GLRenderer.h"
#include "ClassFactory.h"
class Demo
{
public:
	Demo(void);
	void init();
	void run();
	~Demo(void);
private:
	DemoWorld* world;
	Renderer* renderer;
	Controller* controller;
	ClassFactory* factory;
};

