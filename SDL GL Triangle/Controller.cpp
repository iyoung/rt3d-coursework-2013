#include "Controller.h"
#include <SDL.h>

Controller::Controller(void)
{
}
bool Controller::update()
{
	//FIX ME: Needs refactored to UNpress buttons too, currently will lock buffer elements to true.
	Uint8 *keys = SDL_GetKeyboardState(NULL);

	if ( keys[SDL_SCANCODE_ESCAPE]) return false;
	//		camera panning keys
	keyBuffer[STRAFE_L] = keys[SDL_SCANCODE_A] ;
	keyBuffer[STRAFE_R] = keys[SDL_SCANCODE_D] ;
	keyBuffer[TURN_L] = keys[SDL_SCANCODE_Q];
	keyBuffer[TURN_R] = keys[SDL_SCANCODE_E];
	keyBuffer[FORWARD] = keys[SDL_SCANCODE_W];
	keyBuffer[BACKPEDAL] = keys[SDL_SCANCODE_S];
	keyBuffer[PITCH_UP] = keys[SDL_SCANCODE_UP];
	keyBuffer[PITCH_DOWN] = keys[SDL_SCANCODE_DOWN];
	keyBuffer[RUN] = keys[SDL_SCANCODE_LSHIFT];
	keyBuffer[JUMP] = keys[SDL_SCANCODE_SPACE];
	keyBuffer[ASCEND] = keys[SDL_SCANCODE_LCTRL];
	keyBuffer[DESCEND] = keys[SDL_SCANCODE_LALT];
	return true;
}

Controller::~Controller(void)
{
}
