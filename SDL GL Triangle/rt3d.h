#ifndef RT3D
#define RT3D

#include <GL/glew.h>
#include <SDL.h>
#include <iostream>
#include <fstream>
#include <string>

#define RT3D_VERTEX		0
#define RT3D_COLOUR		1
#define RT3D_NORMAL		2
#define RT3D_TEXCOORD   3
#define RT3D_INDEX		4

namespace rt3d {

	struct lightStruct {
		GLfloat ambient[4];
		GLfloat diffuse[4];
		GLfloat specular[4];
		GLfloat position[4];
		void setAmbient(GLfloat R, GLfloat G, GLfloat B, GLfloat A)
		{
			ambient[0]=R;
			ambient[1]=G;
			ambient[2]=B;
			ambient[3]=A;
		}
		void setDiffuse(GLfloat R, GLfloat G, GLfloat B, GLfloat A)
		{
			diffuse[0]=R;
			diffuse[1]=G;
			diffuse[2]=B;
			diffuse[3]=A;
		}

		void setSpecular(GLfloat R, GLfloat G, GLfloat B, GLfloat A)
		{
			specular[0]=R;
			specular[1]=G;
			specular[2]=B;
			specular[3]=A;
		}
		void setPosition(GLfloat X, GLfloat Y, GLfloat Z, GLfloat W)
		{
			position[0]=X;
			position[1]=Y;
			position[2]=Z;
			position[3]=W;
		}
	};

	struct materialStruct {
		GLfloat ambient[4];
		GLfloat diffuse[4];
		GLfloat specular[4];
		GLfloat emissive[4];
		GLfloat shininess;
		
		void setAmbient(GLfloat R, GLfloat G, GLfloat B, GLfloat A)
		{
			ambient[0]=R;
			ambient[1]=G;
			ambient[2]=B;
			ambient[3]=A;
		}
		void setDiffuse(GLfloat R, GLfloat G, GLfloat B, GLfloat A)
		{
			diffuse[0]=R;
			diffuse[1]=G;
			diffuse[2]=B;
			diffuse[3]=A;
		}

		void setSpecular(GLfloat R, GLfloat G, GLfloat B, GLfloat A)
		{
			specular[0]=R;
			specular[1]=G;
			specular[2]=B;
			specular[3]=A;
		}
		void setEmissive(GLfloat R, GLfloat G, GLfloat B, GLfloat A)
		{
			emissive[0]=R;
			emissive[1]=G;
			emissive[2]=B;
			emissive[3]=A;
		}
		void setShininess(GLfloat Ns)
		{
			shininess = Ns;
		}
	};


	void exitFatalError(const char *message);
	char* loadFile(const char *fname, GLint &fSize);
	void printShaderError(const GLint shader);
	GLuint initShaders(const char *vertFile, const char *fragFile);
	// Some methods for creating meshes
	// ... including one for dealing with indexed meshes
	GLuint createMesh(const GLuint numVerts, const GLfloat* vertices, const GLfloat* colours, const GLfloat* normals,
		const GLfloat* texcoords, const GLuint indexCount, const GLuint* indices);
	// these three create mesh functions simply provide more basic access to the full version
	GLuint createMesh(const GLuint numVerts, const GLfloat* vertices, const GLfloat* colours, const GLfloat* normals,
		const GLfloat* texcoords);
	GLuint createMesh(const GLuint numVerts, const GLfloat* vertices);
	GLuint createColourMesh(const GLuint numVerts, const GLfloat* vertices, const GLfloat* colours);
	GLuint loadBitmap(char *fname);
	GLuint loadSkyBoxBitmap(char* fname);
	void setUniformMatrix4fv(const GLuint program, const char* uniformName, const GLfloat *data);
	void setMatrices(const GLuint program, const GLfloat *proj, const GLfloat *mv, const GLfloat *mvp);
	void setLight(const GLuint program, const lightStruct light);
	void setLightPos(const GLuint program, const GLfloat *lightPos);
	void setMaterial(const GLuint program, const materialStruct material);
	void setEmissiveMaterial(const GLuint program, const materialStruct material);

	void drawMesh(const GLuint mesh, const GLuint numVerts, const GLuint primitive); 
	void drawIndexedMesh(const GLuint mesh, const GLuint indexCount, const GLuint primitive);

	void updateMesh(const GLuint mesh, const unsigned int bufferType, const GLfloat *data, const GLuint size);
}

#endif