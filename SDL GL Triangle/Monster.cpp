#include "Monster.h"
#include <cmath>
#include <stack>
#include <glm/gtx/rotate_vector.hpp>

Monster::Monster(void)
{
	mBounds=NULL;
	mOrientateAngle = 0.0;
	mDetectRadius = 10.0;
	mHostile=false;
}
void Monster::setDimensions(float height, float width, float length)
{
	mHeight = height;
	mWidth = width;
	mLength = length;
	initBounds();
}
void Monster::moveForward(clock_t deltaTime)
{
	point3 tPosition = mPosition;
	mPosition+=(mOrientV*(deltaTime/1000.0f)*mSpeed);
	mBounds->update(mPosition,tPosition,0.0,0.0,0.0);
}
void Monster::moveBack(clock_t deltaTime)
{
	point3 tPosition = mPosition;
	mPosition-=(mOrientV*(deltaTime/1000.0f)*(mSpeed*0.5f));
	mBounds->update(mPosition,tPosition,0.0,0.0,0.0);
}
void Monster::moveRight(clock_t deltaTime)
{
	point3 tPosition = mPosition;
	glm::vec3 rightV = glm::vec3(	sin(mOrientateAngle - 3.14f/2.0f),
									0,
									cos(mOrientateAngle - 3.14f/2.0f));
	mPosition+=rightV*(deltaTime/1000.0f)*(mSpeed*0.8f);
	mBounds->update(mPosition,tPosition,0.0,0.0,0.0);
}
void Monster::moveLeft(clock_t deltaTime)
{
	point3 tPosition = mPosition;
	glm::vec3 rightV = glm::vec3(	sin(mOrientateAngle - 3.14f/2.0f),
									0,
									cos(mOrientateAngle - 3.14f/2.0f));
	mPosition-=rightV*(deltaTime/1000.0f)*(mSpeed*0.8f);
	mBounds->update(mPosition,tPosition,0.0,0.0,0.0);

}
void Monster::initGFX(GLuint shader, GLuint texture, GLuint mesh, std::string meshFname, Camera* camera,rt3d::lightStruct* light, rt3d::materialStruct* material)
{
	mShader=shader;
	mTexture=texture;
	mCamera=camera;
	mMesh = mModel.ReadMD2Model(meshFname.c_str());
	mLight = light;
	mMaterial = material;
	mCurrentMode = IDLE;
	currentAnim = mModel.getCurrentAnim();
}
void Monster::moveTo(point3 iPosition)
{
	//move this objects position, and update
	mBounds->update(iPosition,mPosition,0.0f,mOrientateAngle,0.0f);
	mPosition = iPosition;
}
void Monster::moveTo(float iX, float iY, float iZ)
{
	//move this objects position
	point3 tPosition= glm::vec3(iX,iY,iZ);
	mBounds->update(tPosition,mPosition,0.0f,mOrientateAngle,0.0f);
	mPosition = tPosition;
}
void Monster::update(clock_t deltaTime)
{
	//this is for timer related stuff, such as movement based on speed
	mOrientV.x = cos(mOrientateAngle);
	mOrientV.z = sin(mOrientateAngle);
	mOrientV.y = 0.0;
	mBounds->update(mPosition,mPosition,0.0f,mOrientateAngle,0.0f);
	if(isHostile())
	{
		moveForward(deltaTime);
		mModel.Animate(MD2_RUN,deltaTime);
	}
	//update md2 animations based on character State:
	switch(animMode)
	{
	case(IDLE):
		{
			mModel.Animate(MD2_STAND,deltaTime);
			break;
		}
	case(RUNNING):
		{
				mModel.Animate(MD2_RUN,deltaTime);
			break;
		}
	case(JUMPING):
		{
				mModel.Animate(MD2_JUMP,deltaTime);

			break;
		}
	case(DEAD):
		{
			mModel.Animate(MD2_DEATH1,deltaTime);
			break;
		}
	case(ATTACKING):
		{
			if(mModel.getCurrentAnim()!=MD2_ATTACK)
				mModel.Animate(MD2_ATTACK,deltaTime);
			else
				mModel.Animate(deltaTime);
			break;
		}
	}
	//mModel.Animate(currentAnim,deltaTime/1000.0f);
	rt3d::updateMesh(mMesh,RT3D_VERTEX,mModel.getAnimVerts(),mModel.getVertDataSize());
}
void Monster::render(RenderMode renderSelect,glm::mat4 projection)
{
	glUseProgram(mShader);
	rt3d::setUniformMatrix4fv(mShader,"projection",glm::value_ptr(projection));
	//select rendering mode
	//bind our character's texture
	glBindTexture(GL_TEXTURE_2D,mTexture);
	//create our stack and matrices for transformations
	std::stack<glm::mat4> modelview; 
	glm::mat4 model(1.0);
	glm::mat4 view(1.0);
	modelview.push(model);
	mCamera->setViewMatrix(view);

	modelview.top() = glm::rotate(modelview.top(),-90.0f,glm::vec3(1.0,0.0,0.0));
	modelview.top() = glm::rotate(modelview.top(),-90.0f,glm::vec3(0.0,0.0,1.0));
	//convert to radians and rotate by orientation
	modelview.top() = glm::rotate(modelview.top(), mOrientateAngle/ 0.0174532925f,glm::vec3(0.0,0.0,1.0));
	modelview.top() = glm::scale(modelview.top(),glm::vec3(0.05, 0.05, 0.05));

	modelview.push(modelview.top());
	//modelview transformation
	modelview.top() = view * modelview.top();
	//send matrix, light, and material to the graphics card
	rt3d::setUniformMatrix4fv(mShader,"modelview",glm::value_ptr(modelview.top()));
	rt3d::setLight(mShader,*mLight);
	rt3d::setEmissiveMaterial(mShader,*mMaterial);
	//md2 model triangles wind clockwise, so this will compensate
	glCullFace(GL_FRONT);
	//and draw the model
	rt3d::drawMesh(mMesh,mModel.getVertDataCount(),GL_TRIANGLES);
	
	modelview.pop();
	if(renderSelect==DEBUG)
	{
		//if debug mode is active, we want to render the bounding box
		modelview.top() = view * modelview.top();
		rt3d::setUniformMatrix4fv(mShader,"modelview",glm::value_ptr(modelview.top()));
		rt3d::setLightPos(mShader,mLight->position);
		//render bounding box;
		rt3d::drawMesh(mBoundsCube,8,GL_LINES);
	}
	modelview.pop();
	//reset culling mode back to back for dealing with counter clockwise wound triangles
	glCullFace(GL_BACK);
	//and unbind texture
	//glBindTexture(GL_TEXTURE_2D,0);
}
void Monster::rotate(float angle)
{
	//increment orientation by input
	mOrientateAngle += angle;
	mBounds->update(mPosition,mPosition,0.0,mOrientateAngle,0.0);
}
void Monster::initBounds()
{
		//character needs a bounding box as a requirement of implimenting collidable interface.
	if (mBounds!=NULL)
		delete mBounds;
	mBounds = new BoundingBox();
	//generate new vertices which will be used as collision points.
	mBounds->points.push_back(point3(mPosition.x+mWidth/2.0f, mPosition.y+mHeight, mPosition.z+mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x-mWidth/2.0f, mPosition.y+mHeight, mPosition.z+mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x-mWidth/2.0f, mPosition.y,			mPosition.z+mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x+mWidth/2.0f, mPosition.y,			mPosition.z+mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x+mWidth/2.0f, mPosition.y+mHeight, mPosition.z-mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x-mWidth/2.0f, mPosition.y+mHeight, mPosition.z-mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x-mWidth/2.0f, mPosition.y,			mPosition.z-mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x+mWidth/2.0f, mPosition.y,			mPosition.z-mLength/2.0f));
	//as this is a parallel cuboid all we need do is store 3 axes, and normalise them, for accuracy.
	mBounds->axes.push_back(glm::normalize(mBounds->points[0]-mBounds->points[1]));
	mBounds->axes.push_back(glm::normalize(mBounds->points[2]-mBounds->points[4]));
	mBounds->axes.push_back(glm::normalize(mBounds->points[0]-mBounds->points[3]));
	//finally set number of axis to test for this bounding box.
	mBounds->numAxes=mBounds->axes.size();
	mBoundsCube = rt3d::createMesh(8,(GLfloat*)mBounds->points.data());
}
void Monster::applyForce(vector3d iForce)
{
	//simple physics, in case we want to apply forces.
	mV_Vector+=(iForce/mMass);
}
Monster::~Monster(void)
{
}
