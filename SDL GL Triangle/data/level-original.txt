#WORLD DETAILS

#FORMAT AS FOLLOWS
# P is character
# M is monster
# C is collectable
# S is scenery
# all followed by string which is name

# p is position
# f is filename
# o is orientation
# g is gravity
# w is world width x axis
# l is world length z axis
# h is world height y axis
# f is friction on ground.

//world
g 9.98
f  1.0
w 64
h 28
l 64
//entities
L data/mainLight.txt
s data/skybox.txt
P data/character.txt
M data/monster1.txt
M data/monster2.txt
M data/monster3.txt
M data/monster4.txt
C data/collectable1.txt
C data/collectable2.txt
C data/collectable3.txt
C data/collectable4.txt
S data/building1.txt
S data/building2.txt
S data/building3.txt
S data/building4.txt
S data/building5.txt
S data/building6.txt
S data/building7.txt
S data/building8.txt
S data/building9.txt
S data/building10.txt
S data/building11.txt
S data/building12.txt
S data/building13.txt
S data/building14.txt
S data/building15.txt
S data/building16.txt
S data/building17.txt
S data/building18.txt
S data/building19.txt
S data/building20.txt
S data/building21.txt
S data/building22.txt
S data/building23.txt
S data/building24.txt
S data/building25.txt
S data/building26.txt
S data/building27.txt
S data/building28.txt
S data/roadx.txt
S data/roadc1.txt
S data/roadc2.txt
S data/roadc3.txt
S data/roadc4.txt
S data/road1.txt
S data/road2.txt
S data/road3.txt
S data/road4.txt
S data/road5.txt
S data/road6.txt
S data/road7.txt
S data/road8.txt
S data/roadt1.txt
S data/roadt2.txt
S data/roadt3.txt
S data/roadt4.txt