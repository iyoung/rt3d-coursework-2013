#pragma once
#include "renderable.h"
#include "string"
enum UI_ElementType
{
	WARNING_HP, //user on lowhp
	WARNING_TIME, //warns when timer about to end
	NOTIFY_PICKUP_HP, //user picks up hp
	NOTIFY_PICKUP_OTHER, //user picks up other item
	NOTIFY_TIME_START, //timer start message
	NOTIFY_TIME_END, //timer end message
	NOTIFY_HP, // output user hp
	NOTIFY_COLLECTS, //output user collects
	NOTIFY_FPS, //framerate output
	NOTIFY_MSPF, //milliseconds per frame;
};
class UI :
	public Renderable
{
public:
	UI(void);
	//FIX ME: renderable interface implementations
	virtual void render(RenderMode renderSelect);
	void setMessageText(UI_ElementType msgType, std::string message);
	~UI(void);
};

