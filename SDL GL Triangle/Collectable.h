#pragma once
#include "collidable.h"
#include "Renderable.h"
#include "Updatable.h"
#include "ResizableActor.h"
#include "GFX_Object.h"
#include "WorldObject.h"
/////////////////////////////////////////////////////////
//	this class is for collectable items within a world //
//  It's pretty primitive, only boosting character     //
//  stats, but it will suffice for this demo           //
/////////////////////////////////////////////////////////
class Collectable :
	public Collidable,public Renderable,public Updatable,public ResizableActor,public GFX_Object,public WorldObject
{
public:
	Collectable(void);
	//FIX ME: interface implementations
	virtual void render(RenderMode renderSelect, glm::mat4 projection);
	virtual void initGFX(GLuint shader, GLuint texture, GLuint mesh, GLuint numVerts, Camera* camera, rt3d::lightStruct* light,rt3d::materialStruct* material);
	virtual void setDimensions(float height, float width, float length);
	virtual vector3d getVelocity(){return vector3d(0.0,0.0,0.0);}
	virtual void moveTo(point3 iPosition);
	virtual void rotate(float angle);
	virtual vector3d getOrientation();
	virtual void moveTo(float iX, float iY, float iZ);
	virtual void update(clock_t deltaTime);
	virtual BoundingBox* getBounds(){return mBounds;}
	virtual void initBounds();
	virtual float getRadius(){return 0.0f;}
	virtual point3 getLocation(){return mPosition;}
	//functions unique to this class.
	int getHealthBoost() { return mHealthBoost;}
	void setHealthBoost(int setter) { mHealthBoost = setter;}
	int getStrengthBoost() { return mStrengthBoost;}
	void setStrengthBoost(int setter) { mStrengthBoost = setter;}
	float getSpeedBoost() { return mSpeedBoost;}
	void setSpeedBoost(float setter){ mSpeedBoost = setter;}
	void setInUse(bool setter){mInUse = setter;}
	bool isInUse(){return mInUse;}
	~Collectable(void);
private:
	//world related data
	point3 mPosition;
	float mHeight;
	float mWidth;
	float mLength;
	bool mInUse;
	BoundingBox* mBounds;
	//related bonus data
	int mHealthBoost;
	int mStrengthBoost;
	float mSpeedBoost;
	//graphics related data
	float mRotationSpeed;//radians per second. Used for rotating the collectable along the y axis.
	float mCurrentRotationAngle; //used for transformations.
	GLuint mShader;
	GLuint mTexture;
	GLuint mMesh;
	Camera* mCamera;
	rt3d::lightStruct* mLight;
	rt3d::materialStruct* mMaterial;
	GLuint mBoundsCube;
	GLuint mNumVerts;
};

