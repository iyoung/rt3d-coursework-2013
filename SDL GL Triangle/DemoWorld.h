#pragma once
#include <stdio.h>
#include <vector>
#include "Character.h"
#include "Renderer.h"
#include "Controller.h"
#include "Updatable.h"
#include "Camera.h"
#include "Monster.h"
#include "ClassFactory.h"
#include "skybox.h"
////////////////////////////////////////
//			Main Demo class			  //
//			Handles all Demo Logic	  //
////////////////////////////////////////
typedef glm::vec3 point3;
typedef glm::vec3 vector3d;
//this is used to store the boolean result of a collision check,
//and if collision is true, a vector to apply to colliders position
//to ensure no overlap of objects.
struct collisionResult
{
	bool collided;
	vector3d MTV;
	collisionResult(bool didHit, vector3d iMTV):collided(didHit),MTV(iMTV){}
};
struct Projection
{
	float min;
	float max;
	Projection (float iMin,float iMax):min(iMin),max(iMax){}
	bool overlap(Projection& otherProjection)
	{
		//check for overlap
        if(otherProjection.min>min && otherProjection.min<max)
                return true;
        if(otherProjection.max<max && otherProjection.max>min)
                return true;
        //check for containment
        if(min>otherProjection.min && min<otherProjection.max)
                return true;
        if(max<otherProjection.max && max > otherProjection.min)
                return true;
        //else no overlap
        else
                return false;
	}

	//this function is used for to check for overlap on a 1d basis.
	float getOverlap(Projection& otherProjection)
	{
		float o1,o2;
		o1=otherProjection.min-min;
		o2=otherProjection.max-max;
		if (o1>=o2)
		{
			return o2;
		}
		return o1;
	}
};
class DemoWorld:
	public Updatable
{
public:
	DemoWorld(void);
	//takes in timer to calculate deltaTime, and calulate world variables.
	void initWorld();
	virtual void update(clock_t deltaTime);
	void setController(Controller* iController){mController=iController;}
	void setRenderer(Renderer* renderer){mRenderer = renderer;}
	~DemoWorld(void);
private:
	//Helper functions for collision detection
	Projection project(BoundingBox boxToProject, vector3d axis);
	float overlap(Projection first, Projection second);

	//used for ray/box collision. Not needed for this demo, though might be useful for line of sight calculations later.
	//collisionResult pointCollision(point3 point, Collidable* Collidable);

	//used to check if character is in an area, pretty useful, for checking wether SAT checks are necessary
	//might be useful. Will implement if necessary.
	//collisionResult radiusCollision(float radius, point3 location);

	// this is to check if two boxes are actually colliding
	collisionResult boxCollision(BoundingBox Collider, BoundingBox Collidee);
	// updates enemies on target position, target aqcuisition range etc.
	void updateAI();
	//loads up a world from file, using a world factory method.
	void loadWorld(char* worldFileName);

	//objects which have no AI, but are collidable nonetheless.
	//could be buildings or barriers
	//std::vector<Scenery*> mPhysicalObjects;

	//enemies for the player to avoid/fight/whatever.
	std::vector<Monster*> mEnemies;
	std::vector<Collectable*> collectables;
	std::vector<Scenery*> scenery;
	Character* mPlayer;
	//y coordinate for ground, will default to 0.0f
	float mGroundLevel;
	//value of gravity in m/s^2
	float mGravity;
	//pixels to meters ratio.
	float mWorldScale;
	//world dimensions
	float mWorldWidth;
	float mWorldHeight;
	float mWorldLength;
	//main classes for input/output/world object management
	Renderer* mRenderer;
	Controller* mController;
	ClassFactory* mClassFactory;

	//testing code (may make this permanent)
	Skybox* skyBox;
	std::vector<Renderable*> renderList;
	//end testing code

	// all motion is measured in metres per second, so we do movement calculations of 
	// velocity/acceleration divided my 1000 (milliseconds per second) to get metres per ms
	// then multiplied by mDeltaTime to do accurate movement.
	time_t mDeltaTime;
	float fps;
	float msPerFrame;
	float averageFPS;
	int numUpdates;
	float turnSpeed; //degrees per second.
	float mFriction;
	Camera* mCamera;
	float camFollowDistance;
};

