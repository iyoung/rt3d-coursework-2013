#include "Demo.h"
#include <ctime>

Demo::Demo(void)
{
}
void Demo::init()
{
	
	world = new DemoWorld();
	controller = new Controller();
	renderer = new GLRenderer();
	world->setController(controller);
	world->setRenderer(renderer);
	world->initWorld();

}
void Demo::run()
{



	//initialize variables
	glewExperimental = GL_TRUE;
	clock_t lastTime;
	clock_t currentTime;
	clock_t deltaTime;
	currentTime = clock();
	lastTime = currentTime;
	bool running = true; // set running to true
	SDL_Event sdlEvent;  // variable to detect SDL events
	while (running)	{	// the event loop
		currentTime = clock();
		deltaTime = currentTime-lastTime;
		while (SDL_PollEvent(&sdlEvent)) {
			if (sdlEvent.type == SDL_QUIT)
				running = false;
			else
			{
				controller->update();
			}
		}
		world->update(deltaTime);
		if(deltaTime>1.0/60.0f)
		{
			renderer->renderScene();
			lastTime=currentTime;

		}
		
	}


    SDL_Quit();
}

Demo::~Demo(void)
{
}
