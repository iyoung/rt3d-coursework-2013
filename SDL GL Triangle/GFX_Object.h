#pragma once
#include "rt3d.h"
#include "Camera.h"
class GFX_Object
{
public:
	GFX_Object(void);
	virtual void initGFX(GLuint shader, GLuint texture, GLuint mesh, GLuint numVerts, Camera* camera, rt3d::lightStruct* light, rt3d::materialStruct* material)=0;
	virtual ~GFX_Object(void);
};

