#pragma once
#include "renderable.h"
#include "WorldObject.h"
class LightObject :
	public Renderable, public WorldObject
{
public:
	LightObject(void);
	void init();
	virtual void render(RenderMode renderSelect,glm::mat4 projection);
	void setAmbient(float R, float G, float B, float A);
	void setDiffuse(float R, float G, float B, float A);
	void setSpecular(float R, float G, float B, float A);
	void setPosition(float X, float Y, float Z, float W);
	void setSpotDirection(vector3d iDirection);
	rt3d::lightStruct* getLightData(){return light;}
	virtual void move();
	virtual point3d getLocation();
	virtual vector3d getOrientation();
	virtual void moveTo(glm::vec3 iPosition);
	virtual void rotate(float angle);
	virtual void moveTo(float iX, float iY, float iZ);
	~LightObject(void);
private:
	rt3d::lightStruct* light;
	vector3d orientation;
};

