#include "Camera.h"
#include <glm/gtx/transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

Camera::Camera(void)
{
	
	followDistance = 0.0f;
	horizontalAngle = 0.0f;
	verticalAngle = 0.0f;
	rollAngle = 0.0;
	directionV = glm::vec3(	(std::cos(verticalAngle) * std::sin(horizontalAngle)),
							std::sin(verticalAngle),
							(std::cos(verticalAngle) * std::cos(horizontalAngle)));
	// Right vector
	rightV = glm::vec3(	std::sin(horizontalAngle - 3.14f/2.0f),
						0,
						std::cos(horizontalAngle - 3.14f/2.0f));
	upV= glm::cross( rightV, directionV );
}

void Camera::moveForward(float amount)
{
	positionP+=directionV*amount;
}

void Camera::moveRight(float amount)
{
	positionP+=rightV*amount;
}
void Camera::moveUp(float amount)
{
	positionP+=upV*amount;
}
void Camera::rotateYAW(float angle)
{
	horizontalAngle+=angle;
	directionV = glm::vec3(	(std::cos(verticalAngle) * std::sin(horizontalAngle)),
							std::sin(verticalAngle),
							(std::cos(verticalAngle) * std::cos(horizontalAngle)));
	rightV =glm::rotateY(directionV,-angle);
	// Right vector
	rightV = glm::vec3(	std::sin(horizontalAngle - 3.14f/2.0f),
						0,
						std::cos(horizontalAngle - 3.14f/2.0f));
	upV= glm::cross( rightV, directionV );
}
void Camera::rotatePITCH(float angle)
{
	verticalAngle+=angle;
	directionV = glm::vec3(	(std::cos(verticalAngle) * std::sin(horizontalAngle)),
							std::sin(verticalAngle),
							(std::cos(verticalAngle) * std::cos(horizontalAngle)));
	// Right vector
	rightV = glm::vec3(	std::sin(horizontalAngle - 3.14f/2.0f),
						0,
						std::cos(horizontalAngle - 3.14f/2.0f));
	
	upV= glm::cross( rightV, directionV );
}
void Camera::setTargetPos(glm::vec3 lookAt, glm::vec3 offset,float followDistance, float followVAngle, float followHAngle)
{
	//calculate position
	verticalAngle=followVAngle;
	horizontalAngle=followHAngle;
	directionV = glm::vec3(	(std::cos(verticalAngle) * std::sin(horizontalAngle)),
							std::sin(verticalAngle),
							(std::cos(verticalAngle) * std::cos(horizontalAngle)));
	// Right vector
	rightV = glm::vec3(	std::sin(horizontalAngle - 3.14f/2.0f),
						0,
						std::cos(horizontalAngle - 3.14f/2.0f));
	
	
	upV= glm::cross( rightV, directionV );
	positionP = (lookAt-(directionV*followDistance))+offset;
}

void Camera::setViewMatrix(glm::mat4& viewMatrix)
{

	//calculate direction (forward orientation) and right ascension
	directionV = glm::vec3(	(cos(verticalAngle) * sin(horizontalAngle)),
							sin(verticalAngle),
							(cos(verticalAngle) * cos(horizontalAngle)));
	// Right vector
	rightV = glm::vec3(	sin(horizontalAngle - 3.14f/2.0f),
						0,
						cos(horizontalAngle - 3.14f/2.0f));
	
	upV= glm::cross( rightV, directionV );
	//set and create view matrix for modelView calculations
		viewMatrix       = glm::lookAt(
		positionP,				// Camera is here
		positionP+directionV,	// and looks here : at the same position, plus "direction"
		upV						// Head is up (set to 0,-1,0 to upside down)
		);
}
void Camera::rotateROLL(float angle)
{
	rollAngle+=angle;
}
Camera::~Camera(void)
{

}
