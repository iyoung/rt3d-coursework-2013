#pragma once
#include "Renderable.h"
#include "md2model.h"
enum AnimMode
{
	IDLE,
	RUNNING,
	RUNNING_BACK,
	JUMPING,
	FALLING,
	ATTACKING,
	DEAD,
	HURT,
	CELEBRATE
};
class MD2Renderable :
	public Renderable
{
public:
	MD2Renderable(void);
	virtual void initGFX(GLuint shader, GLuint texture,GLuint mesh, std::string meshFname, Camera* camera,rt3d::lightStruct* light, rt3d::materialStruct* material)=0;
	virtual ~MD2Renderable(void);

	void setAnimMode(AnimMode animationType){if(animMode!=animationType)animMode = animationType;}
	protected:
	AnimMode animMode;
};

