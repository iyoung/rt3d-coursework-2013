#pragma once
#include <ctime>
class Mobile
{
public:
	Mobile(void);
	virtual void moveForward(clock_t deltaTime)=0;
	virtual void moveBack(clock_t deltaTime)=0;
	virtual void moveLeft(clock_t deltaTime)=0;
	virtual void moveRight(clock_t deltaTime)=0;
	virtual ~Mobile(void);
};

