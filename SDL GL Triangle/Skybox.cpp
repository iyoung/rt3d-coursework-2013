#include "Skybox.h"
#include "rt3d.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

Skybox::Skybox()
{

}

void Skybox::initGFX(GLuint shader, GLuint texture, GLuint mesh, GLuint numVerts, Camera* newCamera, rt3d::lightStruct* newLight,rt3d::materialStruct* newMaterial)
{
	light = newLight;
	material = new rt3d::materialStruct();
	material->emissive[0]=1.0;
	material->emissive[1]=1.0;
	material->emissive[2]=1.0;
	material->emissive[3]=1.0;
	cam=newCamera;
	shaderProgram = shader;
	// vertices for flat square  X		Y		Z
	GLfloat meshVerts[]={		1.00f,	1.00f,	1.0f,
								-1.00f,	1.00f,	1.0f,
								-1.00f,	-1.00f,	1.0f,
								1.00f,	-1.00f,	1.0f,
							};
	//coords for texture mapping	U	V
	GLfloat texCoords[] = {			1,	0,
									0,	0,
									0,	1,
									1,	1
	};
	GLuint indices[] = {0,1,2,0,2,3};
	//coords for lighting		X		Y		Z
	GLfloat normals[]={			0.0f,	0.0f,	1.0f,
								0.0f,	0.0f,	1.0f,
								0.0f,	0.0f,	1.0f,
								0.0f,	0.0f,	1.0f,
	};
	mVerts = 6;
	mMesh = rt3d::createMesh(mVerts,meshVerts,nullptr,normals,texCoords,mVerts,indices);
}
void Skybox::renderFace(FaceEnum face,glm::mat4 rotMatrix)
{
	//send rotation matrix and bound texture to shader and  
	//render the specified face
	
	
	rt3d::setUniformMatrix4fv(shaderProgram,"modelview",glm::value_ptr(rotMatrix));
	rt3d::setLight(shaderProgram,*light);
	rt3d::setLightPos(shaderProgram,light->position);
	glBindTexture(GL_TEXTURE_2D,textures[face]);
	rt3d::drawIndexedMesh(mMesh,mVerts,GL_TRIANGLES);
	//glBindTexture(GL_TEXTURE_2D,0);
}
void Skybox::setFaceImage(FaceEnum face,char* BMPfname)
{
	textures[face] = rt3d::loadSkyBoxBitmap(BMPfname);
}
void Skybox::render(RenderMode renderSelect, glm::mat4 projection)
{
	glUseProgram(shaderProgram);
	rt3d::setUniformMatrix4fv(shaderProgram,"projection",glm::value_ptr(projection));
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	if (material != NULL)
	{
		//without this we will see nothing
		rt3d::setEmissiveMaterial(shaderProgram,*material);
	}
	glm::mat4 identity(1.0);
	glm::mat4 view(1.0);
	//get view matrix from camera
	cam->setViewMatrix(view);
	//nullify camera movement so box is always positioned centre to camera
	view = glm::translate(view,cam->positionP);
	std::stack<glm::mat4> ModelViewStack;
	ModelViewStack.push(identity); //push identity
	//render front
	ModelViewStack.push(ModelViewStack.top()); 
	ModelViewStack.top()= glm::scale(ModelViewStack.top(),glm::vec3(scale,scale,scale));
	ModelViewStack.top() = view*ModelViewStack.top();
	renderFace(FRONT, ModelViewStack.top());
	ModelViewStack.pop(); //pop ident copy 1
	//render left side
	ModelViewStack.push(ModelViewStack.top()); //push ident copy 1
	ModelViewStack.top()= glm::scale(ModelViewStack.top(),glm::vec3(scale,scale,scale));
	ModelViewStack.top() = glm::rotate(ModelViewStack.top(),-90.0f,glm::vec3(0.0f,1.0f,0.0f));
	ModelViewStack.top() = view*ModelViewStack.top();
	renderFace(LEFT, ModelViewStack.top());
	ModelViewStack.pop(); //pop ident copy 1
	//render back side
	ModelViewStack.push(ModelViewStack.top()); //push ident copy 2
	ModelViewStack.top()= glm::scale(ModelViewStack.top(),glm::vec3(scale,scale,scale));
	ModelViewStack.top() = glm::rotate(ModelViewStack.top(),-180.0f,glm::vec3(0.0f,1.0f,0.0f));
	ModelViewStack.top() = view*ModelViewStack.top();
	renderFace(BACK, ModelViewStack.top());
	ModelViewStack.pop(); //pop ident copy 2
	////render right side
	ModelViewStack.push(ModelViewStack.top()); //push ident copy 3
	ModelViewStack.top()= glm::scale(ModelViewStack.top(),glm::vec3(scale,scale,scale));
	ModelViewStack.top() = glm::rotate(ModelViewStack.top(),90.0f,glm::vec3(0.0f,1.0f,0.0f));
	ModelViewStack.top() = view*ModelViewStack.top();
	renderFace(RIGHT, ModelViewStack.top());
	ModelViewStack.pop(); //pop ident copy 3
	////render top side
	ModelViewStack.push(ModelViewStack.top()); //push ident copy 4
	ModelViewStack.top()= glm::scale(ModelViewStack.top(),glm::vec3(scale,scale,scale));
	ModelViewStack.top() = glm::rotate(ModelViewStack.top(),-90.0f,glm::vec3(1.0f,0.0f,0.0f));
	ModelViewStack.top() = view*ModelViewStack.top();
	renderFace(UP, ModelViewStack.top());
	ModelViewStack.pop(); //pop ident copy 4
	//render bottom side
	//this is only necessary for 6 side skyboxes. Possibly change this to a switch setting, and conditional check before doing bottom frame.
	/*ModelViewStack.top()= glm::scale(ModelViewStack.top(),glm::vec3(scale,scale,scale));
	ModelViewStack.top() = glm::rotate(ModelViewStack.top(),90.0f,glm::vec3(1.0f,0.0f,0.0f));
	ModelViewStack.top() = view*ModelViewStack.top();
	renderFace(DOWN, ModelViewStack.top());*/
	ModelViewStack.pop(); //pop ident
	
	//re-enable depth checking and backface culling so we dont get a hit to performance
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
}
Skybox::~Skybox(void)
{
}
