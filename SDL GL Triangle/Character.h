#pragma once
#include "GFX_Object.h"
#include "collidable.h"
#include "MD2Renderable.h"
#include "Movable.h"
#include "Camera.h"
#include "Updatable.h"
#include "Collectable.h"
#include "WorldObject.h"
#include "Mobile.h"
#include "ResizableActor.h"
#include "Actor.h"

///////////////////////////////////////////////////////
// Concrete implementation of collidable,			 //
// renderable, and movable interfaces.				 //
// Compatable with renderers supporting Renderable	 //
// Game worlds which support movables/collidables	 //
///////////////////////////////////////////////////////


class Character :
	public Collidable,public MD2Renderable,public Movable,public Updatable,
	public GFX_Object, public WorldObject,public Mobile,public ResizableActor, public Actor
{
public:
	Character(void);
	virtual void setDimensions(float height, float width, float length);
	virtual void render(RenderMode renderSelect,glm::mat4 projection);
	virtual void initBounds();
	virtual void moveForward(clock_t deltaTime);
	virtual void moveBack(clock_t deltaTime);
	virtual void moveLeft(clock_t deltaTime);
	virtual void moveRight(clock_t deltaTime);
	virtual void update(clock_t deltaTime);
	virtual void initGFX(GLuint shader, GLuint texture, GLuint mesh, std::string meshFname, Camera* camera, rt3d::lightStruct* light, rt3d::materialStruct* material);
	virtual void initGFX(GLuint shader, GLuint texture, GLuint mesh, GLuint numVerts, Camera* camera, rt3d::lightStruct* light, rt3d::materialStruct* material){;}
	virtual void move();
	virtual vector3d getVelocity(){return mV_Vector;}
	virtual void moveTo(point3 iPosition);
	virtual void moveTo(float iX, float iY, float iZ);
	virtual void rotate(float angle);
	virtual void applyForce(vector3d iForce);
	virtual vector3d getOrientation();
	virtual point3d getLocation(){ return mPosition; }
	virtual BoundingBox* getBounds(){ return mBounds; }
	virtual float getRadius(){ return mDetectRadius; }
	//Actor implementation
	virtual std::string getName(){ return mName; }
	virtual void setName(std::string iName){ mName = iName; }
	virtual int getHealth(){ return mHealth; }
	virtual void setHealth(int iHealth){ mHealth=iHealth; }
	virtual void addHealth(int iHealth){ mHealth+=iHealth; }
	int getNumCollectables(){ return mNumCollectables; }
	virtual void setSpeed(float iSpeed){ mSpeed = iSpeed; }
	virtual float getSpeed(){ return mSpeed; }
	virtual void setMass(float iMass){ mMass = iMass; }
	virtual float getMass(){ return mMass; }
	virtual float getStrength(){ return mStrength; }
	virtual void setStrength(float iStrength){mStrength = iStrength;}
	float getOrientateAngle(){return mOrientateAngle;}
	//apply collectable effects to character
	void addCollectable(Collectable* iCollectable);
	~Character(void);
private:
	std::string mName;
	int mHealth;
	float mSpeed;
	float mOrientateAngle;
	float mStrength;
	float mMass;
	float mHeight;
	float mWidth;
	float mLength;
	int mNumCollectables;
	float mDetectRadius;
	//collision member data
	BoundingBox* mBounds;
	//graphics member data
	GLuint mBoundsCube;
	Camera* mCamera;
	GLuint mTexture;
	GLuint mShader;
	GLuint mMesh;
	GLuint numVerts;
	md2model mModel;
	rt3d::lightStruct* mLight;
	rt3d::materialStruct* mMaterial;
	int currentAnim;
	float camFollowDistance;
	AnimMode mCurrentMode;
	//world data
	point3 mPosition;
	vector3d mV_Vector;
	vector3d mOrientV;
};

