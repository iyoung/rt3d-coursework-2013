#pragma once
#include <GL/glew.h>
#include "rt3d.h"
#include "md2model.h"
#include "Camera.h"
enum RenderMode
{
	WIRE_FRAME,
	DEBUG,
	FILLED
};
////////////////////////////////////////////////
// Base interface for demo objects which will //
// be drawn or used by the graphics system    //
// each object will require a shader, texture //
// a mesh id, and a reference to the active   //
// camera in the scene, for final			  //
// Pre rendinging transformations			  //
////////////////////////////////////////////////
class Renderable
{
public:
	Renderable(void);
	virtual void render(RenderMode renderSelect,glm::mat4 projection)=0;
	virtual ~Renderable(void);
};

