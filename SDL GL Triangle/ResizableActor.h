#pragma once

/////////////////////////////////////////
///Extends Actor Interface			/////
///Allows Actors to be resizable	///// 
/////////////////////////////////////////

class ResizableActor 
{
public:
	ResizableActor(void);
	virtual void setDimensions(float height, float width, float length)=0;
	virtual ~ResizableActor(void);
};

