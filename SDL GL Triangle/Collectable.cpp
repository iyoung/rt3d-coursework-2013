#include "Collectable.h"


Collectable::Collectable(void)
{
	mBounds = NULL;
	mRotationSpeed = 15.0;
	mCurrentRotationAngle = 0.0;
}
void Collectable::setDimensions(float height, float width, float length)
{
	mHeight = height;
	mWidth = width;
	mLength = length;
	initBounds();
}
void Collectable::render(RenderMode renderSelect,glm::mat4 projection)
{
	if(isInUse())
	{
		glm::mat4 model(1.0);
		//set shader to current
		glUseProgram(mShader);
		rt3d::setUniformMatrix4fv(mShader,"projection",glm::value_ptr(projection));
		//set uniform "light"
		rt3d::setLight(mShader,*mLight);
		//set uniform "material"
		rt3d::setEmissiveMaterial(mShader,*mMaterial);
		//bind texture;
		glBindTexture(GL_TEXTURE_2D,mTexture);
		//set up or use existing projection matrix
		glm::mat4 identity(1.0);
		glm::mat4 view(1.0);
		//set up model matrix;
		//rotate PI/4 radians on X Axis
		//Rotate PI/4 radians on Z Axis
		//Rotate mCurrentRotationAngle on Y Axis
		model = glm::scale(identity,glm::vec3(mWidth,mHeight,mLength));
		model = glm::translate(model,mPosition);
		model = glm::rotate(model,45.0f,glm::vec3(1.0,0.0,1.0));
		model = glm::rotate(model,mCurrentRotationAngle,glm::vec3(1.0,1.0,1.0));
		
		mCamera->setViewMatrix(view);
		//set uniform "modelview"
		glm::mat4 modelview = view * model;
		rt3d::setUniformMatrix4fv(mShader,"modelview",glm::value_ptr(modelview));
		rt3d::setLightPos(mShader,mLight->position);
		//render based on render mode (wireframe, debug(with Bounding box) or filled)
		rt3d::drawMesh(mMesh,mNumVerts,GL_TRIANGLES);
		if(renderSelect == DEBUG)
		{
			rt3d::drawMesh(mBoundsCube,8,GL_TRIANGLES);	
		}
	}
}
void Collectable::update(clock_t deltaTime)
{
	mCurrentRotationAngle+=(mRotationSpeed/1000)*deltaTime;
}
void Collectable::initBounds()
{
	//character needs a bounding box as a requirement of implimenting collidable interface.
	if (mBounds!=NULL)
		delete mBounds;
	mBounds = new BoundingBox();
	//generate new vertices which will be used as collision points.
	mBounds->points.push_back(point3(mPosition.x+mWidth/2.0f, mPosition.y+mHeight, mPosition.z+mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x-mWidth/2.0f, mPosition.y+mHeight, mPosition.z+mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x-mWidth/2.0f, mPosition.y,			mPosition.z+mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x+mWidth/2.0f, mPosition.y,			mPosition.z+mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x+mWidth/2.0f, mPosition.y+mHeight, mPosition.z-mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x-mWidth/2.0f, mPosition.y+mHeight, mPosition.z-mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x-mWidth/2.0f, mPosition.y,			mPosition.z-mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x+mWidth/2.0f, mPosition.y,			mPosition.z-mLength/2.0f));
	//as this is a parallel cuboid all we need do is store 3 axes, and normalise them, for accuracy.
	mBounds->axes.push_back(glm::normalize(mBounds->points[0]-mBounds->points[1]));
	mBounds->axes.push_back(glm::normalize(mBounds->points[2]-mBounds->points[4]));
	mBounds->axes.push_back(glm::normalize(mBounds->points[0]-mBounds->points[3]));
	mBounds->update(mPosition,mPosition,0.0,mCurrentRotationAngle,0.0);
	//finally set number of axis to test for this bounding box.
	mBounds->numAxes=mBounds->axes.size();
	mBoundsCube = rt3d::createMesh(8,(GLfloat*)mBounds->points.data());
}
void Collectable::initGFX(GLuint shader, GLuint texture, GLuint mesh, GLuint numVerts, Camera* camera,rt3d::lightStruct* light,rt3d::materialStruct* material)
{
	mShader = shader;
	mTexture = texture;
	mMesh = mesh;
	mCamera=camera;
	mLight = light;
	mMaterial = material;
	mNumVerts = numVerts;
}
void Collectable::rotate(float angle)
{
		//increment orientation by input
	mCurrentRotationAngle += angle;
	mBounds->update(mPosition,mPosition,0.0,mCurrentRotationAngle,0.0);
}
void Collectable::moveTo(glm::vec3 iPosition)
{
	mBounds->update(iPosition,mPosition,0.0f,0.0f,0.0f);
	mPosition = iPosition;
}
void Collectable::moveTo(float iX, float iY, float iZ)
{
	//move this objects position
	point3 tPosition= glm::vec3(iX,iY,iZ);
	mBounds->update(tPosition,mPosition,0.0f,0.0f,0.0f);
	mPosition = tPosition;
}
vector3d Collectable::getOrientation()
{
	glm::vec3 tOrientV;
	//calculate oriantation unit vector based on angle of orientation
	tOrientV.x = cos(mCurrentRotationAngle);
	tOrientV.z = sin(mCurrentRotationAngle);
	tOrientV.y = 0.0;

	return tOrientV;
}

Collectable::~Collectable(void)
{
}
