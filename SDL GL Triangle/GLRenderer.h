#pragma once
#include "renderer.h"
#include <SDL.h>
#include <GL/glew.h>
#include "Skybox.h"
#include "Camera.h"
#include <vector>
/////////////////////////////////////////////////////
//this class follows the following rendering rules://
//		render light							   //
//		render skybox	   						   //
//		render elements							   //
//		render transparencies					   //
//		render UI								   //
/////////////////////////////////////////////////////

class GLRenderer :
	public Renderer
{
public:
	GLRenderer(void);
	virtual void setupRC();
	virtual void init(){;}
	virtual void setRenderMode(RenderMode mode);
	virtual void addElement(RenderType elemType, Renderable* element);
	virtual void renderScene();
	~GLRenderer(void);
private:
	SDL_Window* mWindow;
	SDL_GLContext context;
	Renderable* mSkybox;
	Renderable* mLight;
	Renderable* mUI;
	std::vector<Renderable*> mElements;
	std::vector<Renderable*> mTransparencies;
	RenderMode mSelectedMode;
	GLuint mDefaultShader;
	GLuint mSkyBoxShader;
};

