#pragma once
#include "collidable.h"
#include "MD2Renderable.h"
#include "Movable.h"
#include "Camera.h"
#include "Updatable.h"
#include "Collectable.h"
#include "Mobile.h"
#include "WorldObject.h"
#include "ResizableActor.h"
#include "Actor.h"
/////////////////////////////////////////////////////////////////////
/// Second concrete class, this will represent enemies in the game///
/////////////////////////////////////////////////////////////////////
class Monster :
	public MD2Renderable,public Updatable, public Collidable, public Movable,
	public Mobile,public WorldObject, public Actor, public ResizableActor
{
public:
	Monster(void);
	virtual void setDimensions(float height, float width, float length);
	virtual void moveForward(clock_t deltaTime);
	virtual void moveBack(clock_t deltaTime);
	virtual void moveLeft(clock_t deltaTime);
	virtual void moveRight(clock_t deltaTime);
	virtual void initGFX(GLuint shader, GLuint texture, GLuint mesh, std::string meshFname, Camera* camera,rt3d::lightStruct* light, rt3d::materialStruct* material);
	virtual void initGFX(GLuint shader, GLuint texture, GLuint mesh, Camera* camera, rt3d::lightStruct* light,rt3d::materialStruct* material){;}
	virtual void render(RenderMode renderSelect,glm::mat4 projection);
	virtual void update(clock_t deltaTime);
	virtual void move(){mPosition+=mV_Vector;}
	virtual vector3d getVelocity(){return mV_Vector;}
	virtual point3d getLocation(){return mPosition;}
	virtual vector3d getOrientation(){return mOrientV;}
	virtual void moveTo(glm::vec3 iPosition);
	virtual void rotate(float angle);
	virtual void moveTo(float iX, float iY, float iZ);
	virtual void applyForce(glm::vec3 iForce);
	virtual BoundingBox* getBounds(){return mBounds;}
	virtual void initBounds();
	virtual float getRadius(){return mDetectRadius;}
	void setHostile(){ mHostile=true;}
	void setPassive(){ mHostile = false;}
	bool isHostile(){ return mHostile;}
	virtual std::string getName(){ return mName; }
	virtual void setName(std::string iName){ mName = iName; }
	virtual int getHealth(){ return mHealth; }
	virtual void setHealth(int iHealth){ mHealth=iHealth; }
	virtual void addHealth(int iHealth){ mHealth+=iHealth; }
	virtual void setSpeed(float iSpeed){ mSpeed = iSpeed; }
	virtual float getSpeed(){ return mSpeed; }
	virtual void setMass(float iMass){ mMass = iMass; }
	virtual float getMass(){ return mMass; }
	virtual float getStrength(){ return mStrength; }
	virtual void setStrength(float iStrength){mStrength = iStrength;}
	~Monster(void);
private:
	bool mHostile;
	float mDetectRadius;
	//collision member data
	BoundingBox* mBounds;
	//graphics member data
	GLuint mBoundsCube;
	Camera* mCamera;
	GLuint mTexture;
	GLuint mShader;
	GLuint mMesh;
	md2model mModel;
	rt3d::lightStruct* mLight;
	rt3d::materialStruct* mMaterial;
	int currentAnim;
	AnimMode mCurrentMode;
	//world data
	point3 mPosition;
	vector3d mV_Vector;
	vector3d mOrientV;

	std::string mName;

	int mHealth;
	float mSpeed;
	float mOrientateAngle;
	float mStrength;
	float mMass;
	float mHeight;
	float mWidth;
	float mLength;
};

