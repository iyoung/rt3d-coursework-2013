#pragma once
#include "Renderable.h"
////////////////////////////////////////////////////
// Base class for render objects                  //
// responsibility is for drawing objects in		  //
// the correct order, and in the correct way	  //
// implementations should support wireframe as    //
// well as filled/textured triangles			  //
////////////////////////////////////////////////////

enum RenderType
{
	SKYBOX,
	LIGHT,
	UI,
	OTHER,
	DEFAULT_SHADER,
	SKYBOX_SHADER,
	TRANSPARENCY,
};

class Renderer
{
public:
	Renderer(void);
	virtual void setupRC()=0;
	virtual void init()=0;
	virtual void addElement(RenderType elemType, Renderable* element)=0;
	virtual void setRenderMode(RenderMode mode)=0;
	virtual void renderScene()=0;
	virtual ~Renderer(void);

	
};

