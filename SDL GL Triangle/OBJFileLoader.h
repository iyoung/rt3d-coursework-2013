#pragma once
#include <stdio.h>
#include <math.h>
#include <Windows.h>
#include <string>
#include <algorithm>
#include <fstream>
#include <vector>
#include <iostream>
#include <GL\glew.h>
#include <glm/glm.hpp>

//////////////////////////////////////////////////////////////
// INCOMPLETE CLASS: WILL NOT LOAD *.mtl FILES. DOES NOT	//
// SUPPORT IMAGE FILES, DOES NOT CALCULATE NORMALS			//
//////////////////////////////////////////////////////////////
struct face
{
	int faceNum;
	bool four;
	int edges[4];
	face(int faceN, int edge1, int edge2, int edge3):faceNum(faceN)
	{
		edges[0]=edge1;
		edges[1]=edge2;
		edges[2]=edge3;
		four = false;
	};	
	face(int faceN, int edge1, int edge2, int edge3, int edge4):faceNum(faceN)
	{
		edges[0]=edge1;
		edges[1]=edge2;
		edges[2]=edge3;
		edges[3]=edge4;
		four = false;
	};
};
class OBJFileLoader
{
	
public:
	OBJFileLoader(void);
	int loadOBJ(const char* filename,std::vector<GLfloat> &verts, std::vector<GLfloat> &norms,std::vector<GLint> &indices, std::vector<GLfloat> &texCoords);
	~OBJFileLoader(void);
private:

};

