#include "GLRenderer.h"
#include "rt3d.h"
#include "Renderable.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

GLRenderer::GLRenderer(void)
{
}

void GLRenderer::setupRC()
{

	//std::cout << glGetString(GL_VERSION) << std::endl;
	if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
    rt3d::exitFatalError("Unable to initialize SDL"); 
	  
    // Request an OpenGL 3.0 context.
    // Not able to use SDL to choose profile (yet), should default to core profile on 3.2 or later
	// If you request a context not supported by your drivers, no OpenGL context will be created

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); 

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // Turn on x4 multisampling anti-aliasing (MSAA)
 
    // Create 800x600 window
    mWindow = SDL_CreateWindow("SDL/GLM/OpenGL Demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!mWindow) // Check window was created OK
        rt3d::exitFatalError("Unable to create window");
 
    context = SDL_GL_CreateContext(mWindow); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
	// Required on Windows *only* init GLEW to access OpenGL beyond 1.1
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) { // glewInit failed, something is seriously wrong
		//std::cout << "glewInit failed, aborting." << std::endl;
		exit (1);
	}
	mSelectedMode = FILLED;
}
void GLRenderer::setRenderMode(RenderMode mode)
{
	mSelectedMode = mode;
}
void GLRenderer::addElement(RenderType elemType, Renderable* element)
{
	if(elemType==SKYBOX)
	{
		mSkybox=element;
	}
	if(elemType==LIGHT)
	{
		mLight=element;
	}
	if(elemType==TRANSPARENCY)
	{
		mTransparencies.push_back(element);
	}
	if(elemType==UI)
	{
		mUI = element;
	}
	else
	{
		mElements.push_back(element);
	}
}
void GLRenderer::renderScene()
{
	// clear the screen
		glClearColor(0.0f,0.0f,0.0f,1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glm::mat4 projection(1.0);
		projection = glm::perspective(35.0f,600.0f/800.0f,1.0f,650.0f);
		mSkybox->render(FILLED,projection);
		if(mSelectedMode == WIRE_FRAME)
		{
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			glDisable(GL_CULL_FACE);
		}
		for (size_t i = 0; i < mElements.size();i++)
		{
			mElements[i]->render(mSelectedMode,projection);
		}
		//this part isnt needed for this demo
		//plus transparent objects need sorted by distance from camera, a form of selective z-buffering.
		//for (size_t i = 0; i < mTransparencies.size();i++)
		//{
		//	//mTransparencies[i]->render();
		//}
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glEnable(GL_CULL_FACE);
		//mUI->render();
		SDL_GL_SwapWindow(mWindow);
}
GLRenderer::~GLRenderer(void)
{
	SDL_GL_DeleteContext(context);
    SDL_DestroyWindow(mWindow);
}
