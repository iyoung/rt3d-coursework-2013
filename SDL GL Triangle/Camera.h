#pragma once
#include <glm/glm.hpp>
////////////////////////////////////////////////////////////////////
// This class will deal with all transformations for renderable   //
// classes.	Has settings for moving on it's own					  //
// or following behind a renderable object in world space		  //
////////////////////////////////////////////////////////////////////
class Camera
{
public:
	Camera(void);
	//forwards/backpedal
	void moveForward(float amount);
	//strafing left or right
	void moveRight(float amount);
	//ascend/descend
	void moveUp(float amount);
	//look left or right
	void rotateYAW(float angle);
	// look up or down
	void rotatePITCH(float angle);
	//roll clockwise or anti-clockwise
	void rotateROLL(float angle);
	//this is for setting follow distance. 0.0 for 1st person, above zero for 3rd person.
	void setTargetPos(glm::vec3 lookAt, glm::vec3 offset, float followDistance, float followVAngle, float followHAngle);
	//used for rendering purposes.
	void setViewMatrix(glm::mat4& viewMatrix);
	~Camera(void);
private:
	glm::vec3 positionP;
	glm::vec3 directionV;
	glm::vec3 upV;
	glm::vec3 rightV;
	float horizontalAngle;
	float verticalAngle;
	float rollAngle;
	float followDistance;
	// this class needs access to camera position in order to nullify camera movement.
	//generally coupling of this closeness is bad practive, but rarely will a skybox be used without a camera.
	//and if no skybox, its still no problem.
	friend class Skybox;
};

