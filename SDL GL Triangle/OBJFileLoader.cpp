#include "OBJFileLoader.h"
#include <glm\glm.hpp>
#include <istream>
#include <sstream>
OBJFileLoader::OBJFileLoader(void)
{
}

int OBJFileLoader::loadOBJ(const char* fname,std::vector<GLfloat> &verts, std::vector<GLfloat> &norms,std::vector<GLint> &indices, std::vector<GLfloat> &texCoords)
{
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	std::vector<GLint> normIDs;
	std::vector<GLint> texID;
	std::vector<glm::vec2> rawTexCoords;
	std::string line;
	std::ifstream inFile(fname, std::ios_base::in);
	bool hasTexture=false;
	bool smoothShading;
	if(!inFile.is_open())
	{
		std::cout<<"file not opened"<<std::endl;
		return -1;
	}
	
	while(!inFile.eof())
	{
		char buff[255];
		inFile.getline(buff,255);
		line.assign(buff);
		
		
		//organise data
 		if(line.c_str()[0] == '#')
		{
			continue;
		}
		//read vertices
		if(line.substr(0,2) == "v ")
		{
			std::istringstream s(line.substr(2));
			glm::vec3 v;
			s>>v.x;s>>v.y;s>>v.z;
			vertices.push_back(v);
		}
		if(line.substr(0,3) == "vn ")
		{
			std::istringstream s(line.substr(2));
			glm::vec3 v;
			s>>v.x;s>>v.y;s>>v.z;
			normals.push_back(v);
		}
		if(line.substr(0,3) == "vt ")
		{
			hasTexture=true;
			std::istringstream s(line.substr(2));
			glm::vec2 v;
			s>>v.x>>v.y;
			rawTexCoords.push_back(v);
		}
		if(line.substr(0,3) == "s 1")
		{
			smoothShading=true;
		}
		//read vertex/normal index data
		//this will need to be updated if texcoords are to be used
		//also will need to be updated to handle smooth shading off "s off" scenario
		if(line.substr(0,2) == "f ")
		{
			std::istringstream stream(line.substr(2));
			
			if(hasTexture)
			{
				int v1,v2,v3,n1,n2,n3,t1,t2,t3;
				char tmp;
				//parse vertices & normals to indices, and normalIndices
				//using indices, populate ordered lists of normals and verts
				//parse, using tmp to filter out '/' delimiters
				stream>>v1>>tmp>>t1>>tmp>>n1>>v2>>tmp>>t2>>tmp>>n2>>v3>>tmp>>t3>>tmp>>n3;
				//set all vars to use c++ array indices
				v1--;v2--;v3--;n1--;n2--;n3--;t1--;t2--;t3--;
				indices.push_back(v1);
				indices.push_back(v2);
				indices.push_back(v3);
				texID.push_back(t1);
				texID.push_back(t2);
				texID.push_back(t3);
				normIDs.push_back(n1);
				normIDs.push_back(n2);
				normIDs.push_back(n3);
			}
			else
			{
				int v1,v2,v3,n1,n2,n3;
				char tmp;
				//parse vertices & normals to indices, and normalIndices
				//using indices, populate ordered lists of normals and verts
				//parse, using tmp to filter out '/' delimiters
				stream>>v1>>tmp>>tmp>>n1>>v2>>tmp>>tmp>>n2>>v3>>tmp>>tmp>>n3;
				//set all vars to use c++ array indices
				v1--;v2--;v3--;n1--;n2--;n3--;
				indices.push_back(v1);
				indices.push_back(v2);
				indices.push_back(v3);
				normIDs.push_back(n1);
				normIDs.push_back(n2);
				normIDs.push_back(n3);
			}
		}
		else
		{/*ignore for now, this will be for texcoords and mat data*/}
		line.clear();
	}

	normals.resize(vertices.size(), glm::vec3(0.0, 0.0, 0.0));
	for (size_t i = 0; i < indices.size(); i++)
	{
		verts.push_back(vertices[indices[i]].x);	
		verts.push_back(vertices[indices[i]].y);	
		verts.push_back(vertices[indices[i]].z);	
	}
  	for (size_t i = 0; i < normIDs.size();i++)
	{
		norms.push_back(normals[normIDs[i]].x);
		norms.push_back(normals[normIDs[i]].y);
		norms.push_back(normals[normIDs[i]].z);
	}
	for (size_t i =0;i<texID.size();i++)
	{
		texCoords.push_back(rawTexCoords[texID[i]].x);
		texCoords.push_back(rawTexCoords[texID[i]].y);
	}
  return  0;
}
OBJFileLoader::~OBJFileLoader(void)
{
}
