#pragma once
#include <GL/glew.h>
#include <glm/glm.hpp>
#include "Camera.h"
#include <stack>
#include "rt3d.h"
#include "Renderable.h"
/////////////////////////////////////////////
// This class operates by creating a single//
// flat square mesh, and rotating it to	   //
// the angle of each face, and binding	   //
// seperate textures					   //
/////////////////////////////////////////////

enum FaceEnum
{
	FRONT,
	BACK,
	LEFT,
	RIGHT,
	UP,
	DOWN
};
//FIX ME: Needs a switch for 5/6 sided modes. This class also needs to be changed to implement renderable interface
class Skybox:
	public Renderable
{
public:
	Skybox();
	~Skybox(void);
	virtual void render(RenderMode renderSelect,glm::mat4 projection);
	virtual void initGFX(GLuint shader, GLuint texture, GLuint mesh,GLuint numVerts, Camera* newCamera, rt3d::lightStruct* newLight,rt3d::materialStruct* newMaterial);
	void setSize(float size){scale = size;}
	void setFaceImage(FaceEnum face,char* BMPfname);
private:
	void renderFace(FaceEnum face,glm::mat4 rotMatrix);	
	GLuint indices[6];
	GLint mVerts;
	GLuint textures[6];
	GLuint mMesh;
	GLfloat scale;
	Camera* cam;
	GLuint shaderProgram;
	rt3d::materialStruct* material;
	rt3d::lightStruct* light;
};

