#pragma once
#include <glm/glm.hpp>
//this interface handles all movement, rotation, and acceleration operations
enum RotateType
{
	YAW,
	PITCH,
	ROLL
};
typedef glm::vec3 vector3d;
typedef glm::vec3 point3d;
class Movable
{
public:
	Movable(void);
	virtual void move()=0;
	virtual vector3d getVelocity()=0;
	virtual void applyForce(glm::vec3 iForce)=0;
	virtual ~Movable(void)=0;
};

