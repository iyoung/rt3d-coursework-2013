#pragma once
#include <glm/glm.hpp>
typedef glm::vec3 vector3d;
typedef glm::vec3 point3d;
class WorldObject
{
public:
	WorldObject(void);
	virtual point3d getLocation()=0;
	virtual vector3d getOrientation()=0;
	virtual void moveTo(glm::vec3 iPosition)=0;
	virtual void rotate(float angle)=0;
	virtual void moveTo(float iX, float iY, float iZ)=0;
	virtual ~WorldObject(void);
};

