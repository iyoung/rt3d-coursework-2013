#include "Character.h"
#include <cmath>
#include <stack>
#include <glm/gtx/rotate_vector.hpp>

Character::Character(void)
{
	mBounds=NULL;
	mOrientateAngle  = 0.0;
	mNumCollectables = 0;
	mDetectRadius = 0.0;
	camFollowDistance = 10.0;
	mOrientV.x = std::sin(mOrientateAngle);
	mOrientV.z = std::cos(mOrientateAngle);

}
void Character::setDimensions(float height, float width, float length)
{
	mHeight = height;
	mWidth = width;
	mLength = length;
	mOrientV.y = 0.0;
	initBounds();
}
void Character::moveForward(clock_t deltaTime)
{
	//update position, bounding box and camera.
	point3 tPosition = mPosition;
	mOrientV = glm::vec3(std::sin(mOrientateAngle),0.0,std::cos(mOrientateAngle));
	glm::vec3 rightV = glm::vec3(	std::sin(mOrientateAngle - 3.14f/2.0f),
									0,
									std::cos(mOrientateAngle - 3.14f/2.0f));
	mPosition+=(mOrientV*(deltaTime/1000.0f)*mSpeed);
	mBounds->update(mPosition,tPosition,0.0,mOrientateAngle,0.0);
	//mCamera->moveForward((deltaTime/1000.0f)*mSpeed);
	mCamera->setTargetPos(mPosition,glm::vec3(0.0,1.0,0.0),camFollowDistance,0.0,mOrientateAngle);
	currentAnim = MD2_RUN;
}
void Character::moveBack(clock_t deltaTime)
{
	//update position, bounding box and camera.
	point3 tPosition = mPosition;
	mOrientV = glm::vec3(std::sin(mOrientateAngle),0.0,std::cos(mOrientateAngle));
	glm::vec3 rightV = glm::vec3(	std::sin(mOrientateAngle - 3.14f/2.0f),
									0,
									std::cos(mOrientateAngle - 3.14f/2.0f));
	//numbers are a little slower moving backwards, hence the 0.5f multiplier
	mPosition-=(mOrientV*(deltaTime/1000.0f)*(mSpeed*0.5f));
	mBounds->update(mPosition,tPosition,0.0,mOrientateAngle,0.0);
	//mCamera->moveForward(-(deltaTime/1000.0f)*(mSpeed*0.5f));
	mCamera->setTargetPos(mPosition,glm::vec3(0.0,1.0,0.0),camFollowDistance,0.0,mOrientateAngle);
	currentAnim = MD2_RUN;

}
void Character::moveRight(clock_t deltaTime)
{
	//update position, bounding box and camera.
	point3 tPosition = mPosition;
	mOrientV = glm::vec3(std::sin(mOrientateAngle),0.0,std::cos(mOrientateAngle));
	glm::vec3 rightV = glm::vec3(	std::sin(mOrientateAngle - 3.14f/2.0f),
									0,
									std::cos(mOrientateAngle - 3.14f/2.0f));
	mPosition+=rightV*(deltaTime/1000.0f)*(mSpeed*0.8f);
	mBounds->update(mPosition,tPosition,0.0,mOrientateAngle,0.0);
	//mCamera->moveRight((deltaTime/1000.0f)*(mSpeed*0.8f));
	mCamera->setTargetPos(mPosition,glm::vec3(0.0,1.0,0.0),camFollowDistance,0.0,mOrientateAngle);
	currentAnim = MD2_RUN;
}
void Character::moveLeft(clock_t deltaTime)
{
	//update position, bounding box and camera.

	point3 tPosition = mPosition;
	mOrientV = glm::vec3(std::sin(mOrientateAngle),0.0,std::cos(mOrientateAngle));
	glm::vec3 rightV = glm::vec3(	std::sin(mOrientateAngle - 3.14f/2.0f),
									0,
									std::cos(mOrientateAngle - 3.14f/2.0f));
	mPosition-=rightV*(deltaTime/1000.0f)*(mSpeed*0.8f);
	mBounds->update(mPosition,tPosition,0.0,0.0,0.0);
	//mCamera->moveRight(-(deltaTime/1000.0f)*(mSpeed*0.8f));
	mCamera->setTargetPos(mPosition,glm::vec3(0.0,1.0,0.0),camFollowDistance,0.0,mOrientateAngle);
	currentAnim = MD2_RUN;
}
void Character::update(clock_t deltaTime)
{
	//calculate oriantation unit vector based on angle of orientation
	mOrientV.x = cos(mOrientateAngle);
	mOrientV.z = sin(mOrientateAngle);
	mOrientV.y = 0.0;
	//this call may not be necessary
	mBounds->update(mPosition,mPosition,0.0f,mOrientateAngle,0.0f);
	//update md2 animations based on character State:
	mModel.Animate(currentAnim,deltaTime/200.0f);
	
	//update mesh on graphics card after animation.
	rt3d::updateMesh(mMesh,RT3D_VERTEX,mModel.getAnimVerts(),numVerts);
}
void Character::initGFX(GLuint shader, GLuint texture, GLuint mesh, std::string meshFname, Camera* camera,rt3d::lightStruct* light, rt3d::materialStruct* material)
{
	//set up basic graphics data.
	mShader = shader;
	mTexture = texture;
	mCamera = camera;
	
	mLight = light;
	mMaterial = material;
	currentAnim = 0;
	mMesh = mModel.ReadMD2Model(meshFname.c_str());
	mCurrentMode=IDLE;
	numVerts = mModel.getVertDataSize();
	//mModel.Animate(0,0);
}
void Character::render(RenderMode renderSelect,glm::mat4 projection)
{
	glUseProgram(mShader);
	//select rendering mode
	rt3d::setUniformMatrix4fv(mShader,"projection",glm::value_ptr(projection));

	//bind our character's texture
	glBindTexture(GL_TEXTURE_2D,mTexture);
	//create our stack and matrices for transformations
	std::stack<glm::mat4> modelview; 
	glm::mat4 model(1.0);
	glm::mat4 view(1.0);
	modelview.push(model);
	mCamera->setViewMatrix(view);
	modelview.top() = glm::translate(modelview.top(),mPosition);
	//compenstate for md2 models version of up, and forward
	modelview.top() = glm::rotate(modelview.top(),-90.0f,glm::vec3(1.0,0.0,0.0));
	modelview.top() = glm::rotate(modelview.top(),-90.0f,glm::vec3(0.0,0.0,1.0));
	//convert to radians and rotate by orientation
	modelview.top() = glm::rotate(modelview.top(), mOrientateAngle*57.2957795f,glm::vec3(0.0,0.0,1.0));
	modelview.top() = glm::scale(modelview.top(),glm::vec3(0.05, 0.05, 0.05));
	modelview.push(modelview.top());
	//modelview transformation
	modelview.top() = view * modelview.top();
	//send matrix, light, and material to the graphics card
	rt3d::setUniformMatrix4fv(mShader,"modelview",glm::value_ptr(modelview.top()));
	rt3d::setLight(mShader, *mLight);
	glm::vec4 tmp = modelview.top()* glm::vec4(mLight->position[0],mLight->position[1],mLight->position[2],mLight->position[3]);
	rt3d::setLightPos(mShader,glm::value_ptr(tmp));
	rt3d::setEmissiveMaterial(mShader,*mMaterial);
	//md2 model triangles wind clockwise, so this will compensate
	glCullFace(GL_FRONT);
	//and draw the model
	rt3d::drawMesh(mMesh,numVerts/3.0f,GL_TRIANGLES);
	
	modelview.pop();
	if(renderSelect==DEBUG)
	{
		//if debug mode is active, we want to render the bounding box
		modelview.top() = view * modelview.top();
		rt3d::setUniformMatrix4fv(mShader,"modelview",glm::value_ptr(modelview.top()));
		rt3d::setLightPos(mShader,mLight->position);
		//render bounding box;
		rt3d::drawMesh(mBoundsCube,8,GL_LINES);
	}
	modelview.pop();
	//reset culling mode back to back for dealing with counter clockwise wound triangles
	glCullFace(GL_BACK);
	//and unbind texture
	glBindTexture(GL_TEXTURE_2D,0);
}
void Character::rotate(float angle)
{
	//increment orientation by input angle, in radians
	mOrientateAngle += angle;
	//rotate bounding box by input angle in radians
	mBounds->update(mPosition,mPosition,0.0,angle,0.0);
	//set orientation unit vector, based on total angle, in radians
	mOrientV.x = std::sin(mOrientateAngle);
	mOrientV.z = std::cos(mOrientateAngle);
	//camera motion.
	//move forward to nullify follow distance
	//mCamera->moveForward(camFollowDistance);
	//rotate by angle in radians
	mCamera->setTargetPos(mPosition,glm::vec3(0.0,1.0,0.0),camFollowDistance,0.0,mOrientateAngle);
	//mCamera->rotateYAW(angle);
	//and move back.
	//mCamera->moveForward(-camFollowDistance);
}
void Character::initBounds()
{
	//character needs a bounding box as a requirement of implimenting collidable interface.
	if (mBounds!=NULL)
		delete mBounds;
	mBounds = new BoundingBox();
	//generate new vertices which will be used as collision points.
	mBounds->points.push_back(point3(mPosition.x+mWidth/2.0f, mPosition.y+mHeight, mPosition.z+mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x-mWidth/2.0f, mPosition.y+mHeight, mPosition.z+mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x-mWidth/2.0f, mPosition.y,			mPosition.z+mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x+mWidth/2.0f, mPosition.y,			mPosition.z+mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x+mWidth/2.0f, mPosition.y+mHeight, mPosition.z-mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x-mWidth/2.0f, mPosition.y+mHeight, mPosition.z-mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x-mWidth/2.0f, mPosition.y,			mPosition.z-mLength/2.0f));
	mBounds->points.push_back(point3(mPosition.x+mWidth/2.0f, mPosition.y,			mPosition.z-mLength/2.0f));
	//as this is a parallel cuboid all we need do is store 3 axes, and normalise them, for accuracy.
	mBounds->axes.push_back(glm::normalize(mBounds->points[0]-mBounds->points[1]));
	mBounds->axes.push_back(glm::normalize(mBounds->points[2]-mBounds->points[4]));
	mBounds->axes.push_back(glm::normalize(mBounds->points[0]-mBounds->points[3]));
	mBounds->update(mPosition,mPosition,0.0,mOrientateAngle,0.0);
	//finally set number of axis to test for this bounding box.
	mBounds->numAxes=mBounds->axes.size();
	mBoundsCube = rt3d::createMesh(8,(GLfloat*)mBounds->points.data());
}
vector3d Character::getOrientation()
{
	//return a unit vector representing the orientation of the character
	return mOrientV;
}
void Character::move()
{
	//increment camera and character position by velocity;
	mPosition+=mV_Vector;
	//mCamera->setTargetPos(mPosition,mOrientV*5.0f,1.0,0.0,0.0);
}
void Character::moveTo(point3 iPosition)
{
	//move this objects position, and update
	mOrientV.x = std::sin(mOrientateAngle);
	mOrientV.z = std::cos(mOrientateAngle);
	mOrientV.y = 0.0f;
	mBounds->update(iPosition,mPosition,0.0f,mOrientateAngle,0.0f);
	mPosition = iPosition;
}
void Character::moveTo(float iX, float iY, float iZ)
{
	//move this objects position
	point3 tPosition= glm::vec3(iX,iY,iZ);
	moveTo(tPosition);
}
void Character::applyForce(vector3d iForce)
{
	//simple physics, in case we want to apply forces.
	mV_Vector+=(iForce/mMass);
}
void Character::addCollectable(Collectable* iCollectable)
{
	//get the various bonus' from the input Collectable
	//and apply it to the character
	mHealth+=iCollectable->getHealthBoost();
	mStrength+=iCollectable->getStrengthBoost();
	mSpeed+=iCollectable->getSpeedBoost();
}
Character::~Character(void)
{
	//free up any reserved heap memory
	delete mBounds;
	//as these were passed in as params, they cannot be freed here, but we can remove references to them.
	mMaterial = nullptr;
	mLight = nullptr;
	mCamera = nullptr;
	mModel = nullptr;
}
