#pragma once
enum ButtonType
{
	FORWARD,
	BACKPEDAL,
	STRAFE_L,
	STRAFE_R,
	TURN_L,
	TURN_R,
	JUMP,
	CROUCH,
	RUN,
	PRIMARY_ATTACK,
	SECONDARY_ATTACK,
	TERTIARY_ATTACK,
	INTERACTION,
	DODGE,
	OPTIONS,
	PITCH_UP,
	PITCH_DOWN,
	ASCEND,
	DESCEND,
};
//FIXME: Needs event handling for keyup events
////////////////////////////////////////////////////
// Utility class, used for storing button states  //
// between event callbacks.						  //
// Has generic function to allow host program	  //
// to query valid key states as required          //
////////////////////////////////////////////////////
class Controller
{
public:
	Controller(void);
	bool getButtonState(ButtonType buttonQuery){return keyBuffer[buttonQuery];}
	bool update();
	~Controller(void);
private:
	bool keyBuffer[256];
};

