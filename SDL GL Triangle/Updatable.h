#pragma once
#include <ctime>
class Updatable
{
public:
	Updatable(void);
	virtual void update(clock_t deltaTime)=0;
	virtual ~Updatable(void);
};

