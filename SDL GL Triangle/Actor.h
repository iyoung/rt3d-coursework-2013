#pragma once
#include <string>
class Actor
{
public:
	Actor(void);
	virtual std::string getName()=0;
	virtual void setName(std::string iName)=0;
	virtual int getHealth()=0;
	virtual void setHealth(int iHealth)=0;
	virtual void addHealth(int iHealth)=0;
	virtual void setSpeed(float iSpeed)=0;
	virtual float getSpeed()=0;
	virtual void setMass(float iMass)=0;
	virtual float getMass()=0;
	virtual float getStrength()=0;
	virtual void setStrength(float iStrength)=0;
	virtual ~Actor(void);
};

