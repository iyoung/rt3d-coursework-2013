#include "DemoWorld.h"
#include <glm/glm.hpp>
#include "rt3d.h"
#include "Camera.h"
#include "LightObject.h"
#include <istream>
#include <sstream>

DemoWorld::DemoWorld(void)
{

}
void DemoWorld::initWorld()
{
	mClassFactory = new ClassFactory();
	//1 radian per second.
	turnSpeed = 1.0;
	mRenderer->setupRC();
	mClassFactory->init();
	std::string temp;
	std::string fname;
	//load world from level.txt
	std::ifstream file;
	std::vector<std::string*> fileData;
	mCamera = mClassFactory->getCamera();
	char lineData[256];
	//open the specified file
	file.open("data/level-nomon.txt",std::ios::in);
	if (file.is_open())
	{
		while ( file.good() )
		{
			file.getline(lineData,256);
			fileData.push_back(new std::string(lineData));
		}
	}
	file.close();
	for ( size_t i = 0;i < fileData.size();i++)
	{
		//skybox data
		if(fileData[i]->c_str()[0] == 's')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>fname;
			skyBox=mClassFactory->getSkyBox(fname);
			mRenderer->addElement(SKYBOX,skyBox);
			fname.clear();
		}
		//collectable data
		if(fileData[i]->c_str()[0] == 'C')
		{
			//parse object file name
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>fname;
			Collectable* tempC;
			//request collectable object from factory
			tempC=mClassFactory->getCollectable(fname);
			collectables.push_back(tempC);
			mRenderer->addElement(OTHER,tempC);
			fname.clear();
		}
		//scenery data
		if(fileData[i]->c_str()[0] == 'S')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>fname;
			Scenery* tempC=mClassFactory->getScenery(fname);
			scenery.push_back(tempC);
			mRenderer->addElement(OTHER,tempC);
			fname.clear();
		}
		//enemy data
		if(fileData[i]->c_str()[0] == 'M')
		{
			std::istringstream ss(fileData[i]->c_str());
			ss>>temp>>fname;
			Monster* tempC=mClassFactory->getMonster(fname);
			mEnemies.push_back(tempC);
			mRenderer->addElement(OTHER,tempC);
			fname.clear();
		}
		//player data
		if(fileData[i]->c_str()[0] == 'P')
		{
			std::stringstream ss(fileData[i]->c_str());
			ss>>temp>>fname;
			mPlayer = mClassFactory->getCharacter(fname);
			//mCamera->setTargetPos(mPlayer->getLocation(), mPlayer->getLocation()+(mPlayer->getOrientation()*-10.0f), 0.0,0.0,0.0);
			//mCamera->moveForward(-10.0);
			mRenderer->addElement(OTHER,mPlayer);
		}
		//light object data
		if(fileData[i]->c_str()[0] == 'L')
		{
			std::stringstream ss(fileData[i]->c_str());
			ss>>temp>>fname;
			mRenderer->addElement(LIGHT,mClassFactory->getLight(fname));
			fname.clear();
		}
		//gravity
		if(fileData[i]->c_str()[0] == 'g')
		{
			std::stringstream ss(fileData[i]->c_str());
			ss>>temp>>mGravity;
		}
		//friction
		if(fileData[i]->c_str()[0] == 'f')
		{
			std::stringstream ss(fileData[i]->c_str());
			ss>>temp>>mFriction;
		}
		//world width
		if(fileData[i]->c_str()[0] == 'w')
		{
			std::stringstream ss(fileData[i]->c_str());
			ss>>temp>>mWorldWidth;
		}
		//world height
		if(fileData[i]->c_str()[0] == 'h')
		{
			std::stringstream ss(fileData[i]->c_str());
			ss>>temp>>mWorldHeight;
		}
		//world length
		if(fileData[i]->c_str()[0] == 'l')
		{
			std::stringstream ss(fileData[i]->c_str());
			ss>>temp>>mWorldLength;
		}
		
	}
	//set default ground level
	mGroundLevel = 0.0;
}
Projection DemoWorld::project(BoundingBox boxToProject, vector3d axis)
{
	//normalize incoming axis, for accuracy
	vector3d tempAxis = glm::normalize(axis);
	//project first box vert to axis, and take this as our minimum
	float min = glm::dot(tempAxis,boxToProject.points[0]);
	//we don't know this yet, so set it to zero
	float max=0.0f;
	//now iterate through the remaining verts in the shape
	for(size_t i = 1; i < boxToProject.numAxes;i++)
	{
		
		float p = glm::dot(tempAxis,boxToProject.points[i]);

		if (p < min)
		{
			min=p;
		}
		if (p > max)
		{
			max = p;
		}
	}
	return *new Projection(min, max);
}
void DemoWorld::update(clock_t deltaTime)
{
	//users turn
	if(mPlayer->getHealth()==0)
	{
		mPlayer->setAnimMode(DEAD);
	}
	else
	{
	//move character according to user input from controller
	if(mController->getButtonState(FORWARD))
	{
		mPlayer->moveForward(deltaTime);
		mPlayer->setAnimMode(RUNNING);
	}
	if(mController->getButtonState(BACKPEDAL))
	{
		mPlayer->moveBack(deltaTime);
		mPlayer->setAnimMode(RUNNING_BACK);
	}

	if(mController->getButtonState(TURN_L))
	{
		//rotate by (radians per second/1000) * deltaTime
		mPlayer->rotate(turnSpeed*deltaTime/1000.0f);
		mPlayer->setAnimMode(IDLE);
	}
	if(mController->getButtonState(TURN_R))
	{
		//rotate by (-radians per second/1000) * deltaTime
		mPlayer->rotate(-turnSpeed*deltaTime/1000.0f);
		mPlayer->setAnimMode(IDLE);
	}
	//god mode controls
	//if(mController->getButtonState(ASCEND))
	//{
	//	mPlayer->moveTo(mPlayer->getLocation()+glm::vec3(0.0,1.0,0.0));
	//}
	//if(mController->getButtonState(DESCEND))
	//{
	//	mPlayer->moveTo(mPlayer->getLocation()+glm::vec3(0.0,-1.0,0.0));
	//}
	else
	{
		mPlayer->setAnimMode(IDLE);
	}
	//std::cout<<mPlayer->getLocation().x<<" : "<<mPlayer->getLocation().z<<std::endl;

	//set camera to follow character.
	//run collision detection for player on collectables
	for (size_t i = 0; i < collectables.size(); i++)
	{
		if(collectables[i]->isInUse())
		{
			collisionResult result = boxCollision(*mPlayer->getBounds(),*collectables[i]->getBounds());
			if(result.collided)
			{
				std::cout<<"You got an Item!"<<std::endl;
				mPlayer->addCollectable(collectables[i]);
				//needs UI object to do this.
				//UI->SetMessage(NOTIFY, "You got an Item!");
				collectables[i]->setInUse(false);
			}
		}
	}
	//run collision detection for player on enemies
	//for (size_t i = 0; i < mEnemies.size(); i++)
	//{
	//	collisionResult result = boxCollision(*mPlayer->getBounds(),*mEnemies[i]->getBounds());
	//	if(result.collided)
	//	{
	//		mPlayer->setHealth(mPlayer->getHealth()-1);
	//		//UI->setMessage(HEALTH,mPlayer->getHealth());
	//		//physics for being "hit" by an enemy.
	//		//mPlayer->applyForce(result.MTV*mEnemies[i]->getVelocity());
	//		mPlayer->setAnimMode(HURT);
	//		mEnemies[i]->setAnimMode(DEAD);
	//		//this function doesnt exist yet
	//		//mEnemies[i]->setState(INACTIVE);
	//	}
	//	//collision detection player against scenery
		for(int i = 0; i < scenery.size();i++)
		{
			collisionResult result = boxCollision(*mPlayer->getBounds(),*scenery[i]->getBounds());
			if(result.collided)
			{
				//currently mtv is not calculated properly.
				//mPlayer->moveTo(mPlayer->getLocation()+result.MTV);
			}
		}
	//}
	for (int i = 0; i < collectables.size();i++)
	{
		collectables[i]->update(deltaTime);
	}
	mPlayer->update(deltaTime);
	for(int i = 0; i < mEnemies.size();i++)
	{
		mEnemies[i]->update(deltaTime/10.0f);
	}
	//end users turn
	//ai turn
	//update ai
	//move ai
	//run collision detection on ai and character
	//run collision detection on ai and scenery
	//check player status
	//end ai turn
	//if player is alive
	//update ui timer, (cumulative timer keeps track divides by 1k, and deducts from timer (if greater than 1))
	//update ui player collectables
	//update ui player health
	//update ui notify message (status type message)
	//response to collisions
	//render scene
	//mRenderer->renderScene();
	}
}
collisionResult DemoWorld::boxCollision(BoundingBox Collider, BoundingBox Collidee)
{
	//This algorithm is not efficient for parallel axis cuboids, it can work with any convex 3d shape.
	//ideally, such cuboids should use 3 axes for checks. complex shapes could use more.
	//there are better solutions for very complex geometry.
	collisionResult collision= collisionResult(false, glm::vec3(0.0,0.0,0.0));
        //algorithm
        //if two convex objects are not penetrating,
        //one or more axes exist for which the projection of the objects will not overlap
        //main algorithm
        //get axes for both collidable objects
        //for each axis in collidable1
        //project each collidable onto axis
        //check each projection for overlap
        //if no overlap
        //exit function returning unaltered result
        //repeat for loop for collidable2
        //if no exit condition met, modify result, and calculate MTV
		//then return result.
		float minOverlap=0.0f;
		vector3d MTV;
		vector3d smallest;
		//for each axis in collider
		for (size_t i = 0; i < Collider.numAxes;i++)
		{
			//calling this function gives us the maximum and minimum values of each object when projected onto an axis
			Projection proj1 = project(Collidee,Collider.axes[i]);
			Projection proj2 = project(Collider,Collider.axes[i]);
			//if we have no overlap, an axis of seperation exists, so no collision. return empty collision result
			if(!proj1.overlap(proj2))
			{
				return collision;
			}
			//if we have an overlap
			else
			{
				//and no minimum overlap is set
				if (minOverlap==0.0)
				{
					//set it
					minOverlap = proj1.getOverlap(proj2);
					smallest = glm::normalize(Collider.axes[i]);
				}
				else
				{
					//else if minimum overlap is set, check if current is smaller than stored.
					if(proj1.getOverlap(proj2)<minOverlap)
					{
						//if true, set current as stored and record current axis as the one with the smallest overlap
						minOverlap = proj1.getOverlap(proj2);
						smallest = glm::normalize(Collider.axes[i]);
						MTV=smallest;
					}
				}
			}
		}
		//repeat for second bounding box;
		for (size_t i = 0; i < Collidee.numAxes;i++)
		{
			Projection proj1 = project(Collider,Collidee.axes[i]);
			Projection proj2 = project(Collidee,Collidee.axes[i]);
			//if we have no overlap, an axis of seperation exists, so no collision. return empty collision result
			if(!proj1.overlap(proj2))
			{
				return collision;
			}
			//else we have an overlap
			else
			{
				//so compare to stored overlap, to see if current is less
				if(proj1.getOverlap(proj2)<minOverlap)
				{
					//if so, save it, and store current axis as minimum..
					minOverlap = proj1.getOverlap(proj2);
					MTV=glm::normalize(Collider.axes[i]);
				}
			}
		}
		//if we reach this point, we have an MTV, and a distance, so populate and return the collision result.
		MTV*=minOverlap;
		collision.collided=true;
		collision.MTV=MTV;
		return collision;
}
void DemoWorld::updateAI()
{
	//a very simple ai algorithm
	for (size_t i = 0; i < mEnemies.size();i++)
	{
		glm::vec3 vectorToPlayer;
		//get the distance between this enemy and player
		vectorToPlayer = mPlayer->getLocation()-mEnemies[i]->getLocation();
		float distance = glm::length(vectorToPlayer);
		//if distance is less than 5 metres
		//if player is already hostile
		if(mEnemies[i]->isHostile())
		{
			if (vectorToPlayer != mEnemies[i]->getOrientation())
			{
				//calculate how much the enemy needs to turn, then do so, and move forward towards player
				float angleDifference = atan2(mEnemies[i]->getOrientation().z,mEnemies[i]->getOrientation().x) - atan2(vectorToPlayer.z,vectorToPlayer.x);
				mEnemies[i]->rotate(angleDifference);
			}
		}
		//this is a simple aggression radius check, if less than 5 metres, the player has alerted the enemy.
		if(distance < 5.0)
		{
			mEnemies[i]->setHostile();
		}
		//or else the player has either moved outside agression radius, or not alerted the enemy. either way, put enemy into sleep mode.
		else
		{
			mEnemies[i]->setPassive();
		}
	}
}
DemoWorld::~DemoWorld(void)
{
}
