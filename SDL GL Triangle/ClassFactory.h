#pragma once
#include "Character.h"
#include "Renderable.h"
#include "Monster.h"
#include "Collidable.h"
#include "Scenery.h"
#include "LightObject.h"
#include "WorldObject.h"
#include "Skybox.h"
#include "OBJFileLoader.h"
#include "rt3d.h"
#include "Camera.h"
#include "rt3dObjLoader.h"

///////////////////////////////////////////////////
///Utility class for creating game data types   ///
///Has responsibility for all world objects     ///
///able to load animated and static models		///
///////////////////////////////////////////////////

//TODO: Create file loading code for loading from specified data files.
//finish code for loading obj files and assigning to various static renderables
class ClassFactory
{
public:
	ClassFactory(void);
	void init();
	Character* getCharacter(std::string charFname);
	Monster* getMonster(std::string monsterFname);
	Scenery* getScenery(std::string sceneFname);
	Collectable* getCollectable(std::string collFname);
	LightObject* getLight(std::string lightFname);
	Skybox* getSkyBox(std::string skyboxDirectory);
	Camera* getCamera();
	~ClassFactory(void);
private:
	std::vector<WorldObject*> worldObjects;
	std::vector<Skybox*> skyBoxes;
	//shader location in graphics memory
	GLuint defaultShader;
	GLuint skyboxShader;
	GLuint characterShader;

	// member structures for reading obj data.
	OBJFileLoader* ObjLoader;
	std::vector<GLfloat> meshVerts;
	std::vector<GLfloat> meshNormals;
	std::vector<GLint> meshIndices;
	std::vector<GLfloat> texCoords;
	rt3d::lightStruct* worldLight;
	Camera* worldCamera;
	
};

